const fileExtension = require('file-extension')
const GraphQL = require('graphql');
const request = require('request')
const fs = require('fs')
const PDFParser = require('pdf2json')
var pdfParser = new PDFParser()
const AWS = require('aws-sdk')
const config = require('./config.json')

const configOne = new AWS.Config({
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
    region : config.aws_region
});
AWS.config.update(configOne)
const S3 = new AWS.S3({apiVersion: '2006-03-01'})
const uuid = require('node-uuid')
const awsServerlessExpress = require('aws-serverless-express')
const appServer = require('./app.js')
const binaryMimeTypes = [
  'application/octet-stream',
  'font/eot',
  'font/opentype',
  'font/otf',
  'image/jpeg',
  'image/png',
  'image/svg+xml'
]



const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLInt,
  GraphQLFloat,
  GrapqhQLNonNull
} = GraphQL;

const PrinterType = require('./graphql/types/Printer');
const CommandType = require('./graphql/types/Commands');
const CustomerType = require('./graphql/types/Customer');

const PrinterResolvers = require('./graphql/Resolvers/PrinterResolvers')
const CommandResolver = require('./graphql/Resolvers/CommandResolver')

const PrinterService = require('./graphql/dbservice/PrinterService')
const CustomerService = require('./graphql/dbservice/CustomerService')

const defUrl = "https://s3.us-east-2.amazonaws.com/everyprint-printer-files/universal_fighting_engine_ufe.pdf"
const serverAp = require('graphql-server-lambda');

/*PrinterService.notifyTip("bdfddd50-a3fb-11e8-8ea7-6f1ebae5ab30","PARRAIN","a8bb7680-b5eb-11e8-b629-4707eafe0173",function(data){
    console.log(JSON.parse(data))
  })*/

/*var pages = 0
function readPages(file)
{
  fs.readFile(file,'utf8',function(err,data){
    return pages 
  })
}

pdfParser.on('pdfParser_dataReady', function(data) {
  // var doc = data.PDFJS && data.PDFJS.pdfDocument && data.PDFJS.pdfDocument.numPages;
  //console.log(data.formImage.Pages.length)
  
  fs.writeFile("./benzelpages",data.formImage.Pages.length+"")
  return data.formImage.Pages.length
});

function readPage(file){
  fileStream = fs.createWriteStream("benzel.pdf")
  request(defUrl).pipe(fileStream).on('finish',function(){
    pdfParser.loadPDF("benzel.pdf")
    setTimeout(readPages,2000,'./benzelpages')
    
})
}
readPage("benzel.pdf")
*/
/*
var params = {
  "color" : "Couleur",
  "copies" : "3",
  "messengerid" : "1965829820097941",
  "url" : defUrl,
  "pages_to_print" : "all",
  "paper_format" : "A4",
  "grouping" : "Reliure",
  "name" : "Ben",
  "rectoverso" : "Oui"
}

CommandResolver.computeCommand(params,null,function(res){
  console.log("Sending price "+params)
  console.log(".... DONE ")
  messages = JSON.parse(res)
  console.log(JSON.stringify({
    "messages" : messages.messages
  }))
})
*/


// This method just inserts the user's first name into the greeting message.
const getGreeting = firstName => `Hello, ${firstName}.`

// Here we declare the schema and resolvers for the query
const schema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'RootQueryType', // an arbitrary name
    fields: {
      // the query has a field called 'greeting'
      greeting: {
        // we need to know the user's name to greet them
        args: { firstName: { name: 'firstName', type: new GraphQL.GraphQLNonNull(GraphQL.GraphQLString) } },
        // the greeting message is a string
        type: GraphQLString,
        // resolve to a greeting message
        resolve: (parent, args) => { getGreeting(args.firstName)}
      },
      printer : {
        type : PrinterType,
        description : "The printer's information based on his id",
        args : {
          printer_id : {
            type : new GraphQL.GraphQLNonNull(GraphQL.GraphQLString),
            description : "The id of the printer"
          }
        },
        resolve : (parent,args,context,info) => {
          return new Promise((resolve,reject)=>{
            PrinterResolvers.getById(args,function(data){
            console.log(data.name+data.printer_id)
              resolve(data)
           });
          });
        }
      }
    }
  }),
})
module.exports.graphql = function(event, context, callback)  {
  const callbackFilter = (err, output) => {
    output.headers['Access-Control-Allow-Origin'] = '*';
    output.headers['Access-Control-Allow-Credentials'] = true // Required for cookies, authorization headers with HTTPS

    callback(err, output);
  };
  const handler = serverAp.graphqlLambda({
    schema: require('./app.js')
  });
  return handler(event, context, callbackFilter);
};

module.exports.graphiql = serverAp.graphiqlLambda({
  endpointURL: '/dev/graphql'
});



module.exports.appServerListen = (event, context, callback) => {
  
  event.headers['Access-Control-Allow-Origin'] = '*';
  event.headers['Access-Control-Allow-Credentials'] = true;
  const server = awsServerlessExpress.createServer(appServer)

  awsServerlessExpress.proxy(server, event, context)
  
}

module.exports.query = (event, context, callback) => graphql(schema, event.queryStringParameters.query)
  .then( result => callback(null, {
                statusCode: 200,
                body: JSON.stringify(result)
              }),
    err => callback(err)
  )
  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
module.exports.ispdf = (event, context, callback) => {
  result = (fileExtension(event.queryStringParameters.fileName)=="pdf")?"yes":"no"
  callback(null,{statusCode : 200, body: JSON.stringify(result)})
};

module.exports.validatePrinter = (event, context, callback) => {
  const printer_id = event.queryStringParameters.printer_id
  const token = event.queryStringParameters.token
  PrinterService.validate(printer_id,token,function(data){
    if(JSON.parse(data).codeReturn == "1"){
      callback(null, {
        statusCode : 200,
        body : {
          "verified" : true,
          "printer_id" : printer_id
        }
      })
    }else{
      callback(null, {
        statusCode : 200,
        body : {
          "verified" : false,
          "printer_id" : printer_id
        }
      })
    }
  });
}

module.exports.handleCommandLaunch = (event,context, callback) => {
  
  var details = {
    "messenger_id" :  event.queryStringParameters.messenger_id,
    "command_id" : event.queryStringParameters.command_id,
    "paid" : event.queryStringParameters.paid,
    "color" : event.queryStringParameters.color,
    "copies" : event.queryStringParameters.copies,
    "range" : event.queryStringParameters.range,
    "grouping" : event.queryStringParameters.grouping, 
    "paper" : event.queryStringParameters.paper, 
    "sent" : event.queryStringParameters.sent,
    "name" : event.queryStringParameters.name, 
    "fileUrl" : event.queryStringParameters.file_url
  }

  /**
  ** Create the command with the printer_id 
  ** For test Purpose, printer_id = 5 (le parrain)
  **/


  CommandResolver.saveCommand(details.color, 
                       details.copies,
                       details.messenger_id,
                       details.fileUrl,
                       details.range,
                       details.paper,
                       details.grouping,
                       function(saveResult){
                        /**
                        ** Did command creation succeeded ??
                        **/
                        result = JSON.parse(saveResult)
                        if(result.codeReturn == "1"){
                          
                        }else{
                          callback(null,{
                            statusCode : 200,
                            body : {
                              "codeReturn" : "1"
                            }
                          })
                        }
                       })
}

module.exports.updatePhoto = (event,context, callback) => {
  var request = event.body.uploadLogo
  var base64String = request.base64String

  var buffer = new Buffer(base64String,'base64')
  var fileMime = fileType(buffer)
  getFile(fileMime, buffer, function(params){
    S3.putObject(params, function(err,data){
      if(err) {
        callback(null,{
          statusCode : 200,
          body : {
            "error" : "Error during S3 upload",
            "code" : err
          }

        })
      }else{
        PrinterService.updateLogo(event.body.printer_id,data.Location,function(data){
          if(JSON.parse(data).codeReturn == "1"){

            callback(null,{
              statusCode : 200,
              body : {
                "codeReturn" : "1",
                "logo_url" : data.Location
              }
            })
          }
          else{

            callback(null,{
              statusCode : 200,
              body : {
                "codeReturn" : "0",
                "logo_url" : null
              }
            })
          }
        })

      }
    })
  })


}

/**
 * Function to update the state of a command 
 * The url of this lambda function is used in the email sent to the printer
 */

 module.exports.updateCommandState = (event, context, callback) => {
   const command_id  = event.queryStringParameters.command_id 
   const state = event.queryStringParameters.state 

   CommandResolver.updateState(command_id, state, function(result){
     callback(null,{
       body : {
         "codeReturn" : "1",
         "command_id" : command_id,
         "state" : state,
         "message" : "Command state has been well updated"
       }
     })
   })

 }

module.exports.createTip = (event,context,callback) => {
        var parseEvent = JSON.parse(event.body)

        var name = parseEvent.name
        var description = parseEvent.description
        var printer_id = parseEvent.printer_id 
        var url = parseEvent.url 
        
        PrinterService.createTip(printer_id,description,url,name,function(data){
          if(JSON.parse(data).codeReturn == "1"){

            callback(null,{
              statusCode : 200,
               headers : {
              
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
    
              },
              body : JSON.stringify({
                "codeReturn" : "1",
                "id" : JSON.parse(data).tip_id,
                "name" : name,
                "printer_id" : printer_id,
                "description" : description,
                "url" : url 
              })
            })
          }
          else{

            callback(null,{
              statusCode : 200,
              headers : {
              
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
    
              },
              body : JSON.stringify({
                "codeReturn" : "0",
                "id" : null
              })
            })
          }
        })
}

module.exports.tipMessage = (event, context, callback) => {
  //console.log("Printer name "+event.queryStringParameters.printer_name)
  //console.log("\ntip id "+event.queryStringParameters.tipid)
  //console.log("\nparams "+JSON.stringify(event.queryStringParameters))

  PrinterService.sendTipData(event.queryStringParameters.printer_name,event.queryStringParameters.tipid,function(message){
    //console.log("Send Tip Data well done")
   // console.log("Message = "+message)
    if(JSON.parse(message).codeReturn == "1"){
      callback(null,{
        statusCode : 200,
        body : message 
      })
    }else{
      callback(null,{
        statusCode : 200,
        body : JSON.stringify({
          "codeReturn" : "0"
        })
      })
    }
  })
}

module.exports.reviewTip = (event,context,callback)=>{
  CustomerService.addTipFeedback(event.queryStringParameters.customer_id,event.queryStringParameters.tip_id,event.queryStringParameters.review,function(result){
    if(JSON.parse(result).codeReturn == "1"){
      var params = {
          url : "https://api.chatfuel.com/bots/"
                + config.EVEYPRINT_BOT_ID +"/users/"
                + event.queryStringParameters.customer_id + "/send",
          method : 'POST',
          qs : {
            "chatfuel_token" : config.CHATFUEL_API_KEY,
            "chatfuel_block_name" : config.EVERYPRINT_TIP_REVIEW_OK
          }
        }

        request(params, function(error, response, body){
          if(error){
            console.log("Not well achieved")
            callback(null,{
              "statusCode" : 200,
              "codeReturn" : "0",
              "message" : "Error during post",
              "body" : body
            });
        }else{
          console.log("Well done")
          callback(null,{
            statusCode : 200,
            codeReturn : "1",
            headers : {

                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            message : "Well uploaded",
            body : body
           })
        }
        })
    }
  })
}

module.exports.notifyTip = (event, context, callback) => {
  var printer_id = JSON.parse(event.body).printer_id
  var printer_name = JSON.parse(event.body).printer_name
  var tip_id = JSON.parse(event.body).tip_id


  PrinterService.notifyTip(printer_id,printer_name,tip_id,function(data){
    data = JSON.parse(data)
    if(data.codeReturn == "1"){
      
      callback(null,{
            statusCode : 200,
            codeReturn : "1",
            headers : {

                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            message : "Well uploaded",
            body : JSON.stringify({
              "codeReturn" : "1",
              "message" : "Tip well sent to users"
            })
        })

    }else{

      callback(null,{
            statusCode : 200,
            codeReturn : "1",
            headers : {

                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            message : "Well uploaded",
            body : JSON.stringify({
              "codeReturn" : "0",
              "message" : "Error during the tip's notification"
            })
        })
    }
  })
}

module.exports.signedUrl = (event,context,callback) => {
  var nameUrl = uuid.v1()
  var params = {
    Bucket : config.S3_BUCKET,
    Key: nameUrl+"."+JSON.parse(event.body).type.split("/")[1],
    ContentType : JSON.parse(event.body).type,
    ACL: 'public-read',
    Expires: 120
 };
    S3.getSignedUrl('putObject', params, function (err, url) {
     if (err) {
         callback(err);
      }
     else {
      //Blob element 
      //Let's directly upload the element 
         callback(null, {
           statusCode : 200,
           headers : {
              
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Allow-Headers' : 'X-Requested-With'
    
           },
           body : JSON.stringify({
            "uploadURL" : url,
            "name" : nameUrl+"."+JSON.parse(event.body).type.split("/")[1]
          })
    })
  }
  });

}

module.exports.uploadNextBlock = (event,context,callback) => {
  
 
        var params = {
          url : "https://api.chatfuel.com/bots/"
                + config.EVEYPRINT_BOT_ID +"/users/"
                + event.queryStringParameters.id + "/send",
          method : 'POST',
          qs : {
            "chatfuel_token" : config.CHATFUEL_API_KEY,
            "chatfuel_block_name" : config.EVERYPRINT_UPLOAD_NEXT_BLOCK,
            "url" : event.queryStringParameters.url 
          }
        }

        request(params, function(error, response, body){
          if(error){
            console.log("Not well achieved")
            callback(null,{
              "statusCode" : 200,
              "codeReturn" : "0",
              "message" : "Error during post",
              "body" : body
            });
        }else{
          console.log("Well done")
          callback(null,{
            statusCode : 200,
            codeReturn : "1",
            headers : {

                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            message : "Well uploaded",
            body : body
           })
        }
        })

}

module.exports.sendEmail = (event,context,callback) => {
    const subject = 'NEW PRINTING ORDER '+event.queryStringParameters.command_id

    var transporter = nodemailer.createTransport({
      service : 'gmail',
      auth : {
        user : config.everyprint_mail,
        pass : config.everyprint_pass
      }
    });
}


module.exports.createAvailFile = (event,context,callback) => {

  //Upload the file
  var request = event.body.uploadFile
  var base64String = request.base64String

  var buffer = new Buffer(base64String,'base64')
  var fileMime = fileType(buffer)
  getFile(fileMime, buffer, function(params){
    S3.putObject(params, function(err,data){
      if(err) {
        callback(null,{
          statusCode : 200,
          body : {
            "error" : "Error during S3 upload",
            "code" : err
          }

        })
      }else{
        var name = event.body.name
        var validity = event.body.validity
        var printer_id = event.body.printer_id
        PrinterService.createAvailFile(printer_id,name,data.Location,validity,function(data){
          if(JSON.parse(data).codeReturn == "1"){

            callback(null,{
              statusCode : 200,
              body : {
                "codeReturn" : "1",
                "id" : avail_id,
                "name" : name,
                "printer_id" : printer_id,
                "validity" : validity,
                "url" : data.Location
              }
            })
          }
          else{

            callback(null,{
              statusCode : 200,
              body : {
                "codeReturn" : "0",
                "id" : null
              }
            })
          }
        })

      }
    })
  })


}

module.exports.computeCommandBis = (event,context, callback) => {
  CommandResolver.sendPrice(event.queryStringParameters.color, event.queryStringParameters.copies,event.queryStringParameters.messengerid,event.queryStringParameters.url,event.queryStringParameters.pages_to_print,event.queryStringParameters.paper_format,event.queryStringParameters.grouping,event.queryStringParameters.name,event.queryStringParameters.rectoverso,function(res){
    var result = JSON.parse(res)
    //console.log("Messages : result.messages")
    //console.log("Messages : "+result.messages+"\nset_attributes : "+result.set_attributes)
    callback(null,{
      statusCode : 200,
      body : JSON.stringify({
        "set_attributes" : result.set_attributes,
        "messages" : result.messages
      })
    })
  })
}

module.exports.computeCommand = (event,context,callback) => {
  var commandsOptions = {
    "fileUrl" : event.queryStringParameters.fileUrl,
    "range" : event.queryStringParameters.range,
    "copies" : event.queryStringParameters.copies,
    "paper_format" :  event.queryStringParameters.paper_format,
    "grouping" : event.queryStringParameters.grouping,
    "messenger_id" : event.queryStringParameters.messengerid,
    "pages_to_print" : event.queryStringParameters.pages_to_print,
    "color" : event.queryStringParameters.color,
    "name" : event.queryStringParameters.name,
    "rectoverso" : event.queryStringParameters.rectoverso
  }

  var location = {
    "latitude" : event.queryStringParameters.latitude,
    "longitude" : event.queryStringParameters.longitude
  }

  CommandResolver.computeCommand(commandsOptions, location, function(messages){
    messages = JSON.parse(messages)
    console.log(messages)
    callback(null,{
      statusCode : 200,
      body: JSON.stringify({
        "messages" : messages.messages
      })
    })
  })

}

module.exports.saveCustomer = (event,context,callback) => {

  var customerId = event.queryStringParameters.customerId
  var last_name = event.queryStringParameters.last_name
  var gender = event.queryStringParameters.gender

  CustomerResolver.saveCustomer(customerId,last_name,gender,function(data){
    callback(null,{
      statusCode : 200,
      body : {
        "message" : "Well Saved"
      }
    })
  })

}

module.exports.tipFeedback = (event,context,callback) => {
  var customerId = event.queryStringParameters.messenger_id
  var tip_id = event.queryStringParameters.tip_id
  var feedback = event.queryStringParameters.feedback

  CustomerResolver.addTipFeedback(customerId,tip_id,feedback, function(data){
    callback(null,{
      statusCode: 200,
      body : {
        "message" : "Well received"
      }
    })
  })
}

module.exports.reserveAvailFile = (event,context,callback) => {
  var avail_id = event.queryStringParameters.avail_id


}


function getFile(fileMime,buffer,fn){
    var fileExt =   fileMime.ext
    let hash = sha1(new Buffer(new Date().toString()))
    let filePath = hash + '/'
    let fileName = uuid.v1()+'.'+fileExt
    let fileFullName = filePath + fileName

    var params = {
      Bucket : config.S3_BUCKET,
      Key : fileFullName,
      Body : buffer
    };

    var uploadSize = {
      size : buffer.toString('ascii').length,
      type : fileMime.mime,
      name : fileName
    }

    fn(params)

}

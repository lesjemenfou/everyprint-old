Welcome to EveryPrint!
===================

### Create a printer ###
``` javascript
mutation{
  createPrinter(input:
    {
    		mail: "djobiii2078@gmail.com",
      	telephone : "+33695396684",
      	name : "DJOBIII",
      	location : "3.123,4.21",
      	secret_key : "toto"
    }
  ){
    email,
    printer_id
  }
}
```

* This has the effect to create the printer with default parameters such as prices and balance, his telephone number, his name (name of his store), his secret key , his location (latitude,longitude that can be obtained using a google maps api).
* This returns the email of the printer and the auto-generated printer's id. An email is sent to the printer in order to activate his account. The results are on the two figures below : 

![](https://s3.us-east-2.amazonaws.com/everyprint-printer-files/savePrinter.png)
![](https://s3.us-east-2.amazonaws.com/everyprint-printer-files/email.png)

### Query a printer's information ###

``` javascript
{
  printer(printer_id:"xxxxxxx"){
    name,
    printer_id,
    mail,
    telephone,
    location,
    prices{
      colorA4,
      colorA3,
      colorA5,
      serseau,
      grayA4,
      grayA3,
      rayure
    },
    balance,
    logo_url ,
    balance_history{
      date_time,
      retrieved
    },
    commands(state:"1"){
      state,
      color,
      copies,
      sent_time,
       file_url,
       customer{
        last_name,
        gender ,
        telephone
      },
    },
    tips{
      tipid,
      name,
      description,
      tip_url,
      name,
      stats{
        goodReviews,
        neutralReviews,
        badReviews
      }

    },
    available,
    available_files{
      name,
      reserve,
      file_url,
      validity
    },
     per_day_stat {
      day,
      commands_done,
      balance
    }


  }
}
```

* This retrieves all the information about a printer of id **xxxxx**.
* On the prices, you can choose to display some of the fields corresponding to the prices you wish to view
* **balance_history** corresponds to the retrieval history made by the printer to forward his/her balance into his/her account, you can pass the parameter **days** to indicate when the history should stop looking (5 days corresponding to  the history till 5 days)
* **commands** corresponds to the commands the printer is in charge of, **state** precise the type of commands you want to have, if not specified, it retrieves all the commands, state 1->pending commands, 2-> achieved commands , 3-> retrieved commands by customer.
* In the command field, the customer indicates, the information about the **customer** who sent the command, by this way you can obtain, the name, gender and telephone number of the customer.
* **tips** corresponds to the tips created by the printer, you can pass a parameter **number** to precise the number of tips to retrieve, for each tip, you have the statistics **stats** on the tips which corresponds to the feedback given by the  customers, the number of good, neutral and bad reviews
* **available** corresponds to if the printer is available or not to receive new commands. 0 stands for not available and 1 for available
* **available_files** corresponds to the files for which the printer notifies their availability to the customers, each available file has a **reserve** value which corresponds to the number of reservation made by customers enabling the  printer to know the exact amount (approximately), to reserve for a given file
* **per_day_stat** corresponds to the statistics about the number of commands done, balance achieved for a given day. You can precise a parameter **day** to get the statistics of a given day or **number** to indicate the number of days  where the history should stop.

* _The result of the above command is given below :_

![](https://s3.us-east-2.amazonaws.com/everyprint-printer-files/toview.png)

### Update the state of a command ###
``` javascript
mutation{
  updateCommandState(input:{
    command_id:"f8b07320-6753-11e8-805a-ad477ecb3db0",
    state:"2"
  }){
    codeReturn,
    message
  }
}
```

This has for effect to change the **state** of the command whose id is **f8b07320-6754-11e8-805a-ad477ecb3db0** to change to 2. 0 stands for created, 1 stands for achieved (point of view of the printer) and 2 stands for retrieved. When the state changes to 2, a message is automatically sent to the messenger_id of the customer who issued the command.

![](https://s3.us-east-2.amazonaws.com/everyprint-printer-files/upState.png)


More is to come :)
===================

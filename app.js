const express = require('express');
const graphqlHTTP = require('express-graphql');
const GraphQL = require('graphql');

const {
  GraphQLSchema,
	GraphQLObjectType,
	GraphQLString,
	GraphQLID,
	GraphQLInt,
  GraphQLFloat,
	GrapqhQLNonNull
} = GraphQL;

const PrinterType = require('./graphql/types/Printer');
const CommandType = require('./graphql/types/Commands');
const CustomerType = require('./graphql/types/Customer');

const PrinterResolvers = require('./graphql/Resolvers/PrinterResolvers')
const PrinterMutation = require('./graphql/Mutation/PrinterMutation')

var app = express();
app.set('view engine', 'ejs');


// GraphqQL server route

const Query = new GraphQL.GraphQLObjectType({
  name : "EveryPrintBackend",
  description : "The printer's information based on his id",
  fields : {
    printer : {
      type : PrinterType,
      description : "The printer's information based on his id",
      args : {
        printer_id : {
          type : new GraphQL.GraphQLNonNull(GraphQL.GraphQLString),
          description : "The id of the printer"
        }
      },
      resolve : (parent,args,context,info) => {
        return new Promise((resolve,reject)=>{
          PrinterResolvers.getById(args,function(data){
          console.log(data.name+data.printer_id+args)
            resolve(data)
         });
        });


      //  return {
        //  "name" : "LE PARRAIN",
          //"mail" : "djobiii2078@gmail.com",
        //  "printer_id" : "xxxxxxx"
        //}
      }
    }
  }
});



const schema =new GraphQL.GraphQLSchema({
  query : Query,
  mutation : PrinterMutation
});

/*
app.use('/', graphqlHTTP(req => ({
  schema,
  pretty: true,
  graphiql : true
})));
*/
module.exports = schema

// start server
/*
var server = app.listen(8080, () => {
  console.log('Listening at port', server.address().port);
});
*/
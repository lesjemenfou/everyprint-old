const AWS = require('aws-sdk')
const config = require('../../config.json')

const configOne = new AWS.Config({
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
    region : config.aws_region
});

AWS.config.update(configOne);
const crypto = require('crypto')
const util = require('util')
const uuid = require('node-uuid')
const S3 = new AWS.S3()
const dynamodb = new AWS.DynamoDB()
const docuClient = new AWS.DynamoDB.DocumentClient()




module.exports = {

  /**
  ** Save a user in dynamodb table
  ** @customer json containing customer info
  ** Return messenger_id if successful
  **/

save : (customer,fn) => {
  dynamodb.putItem({
    TableName : config.CUSTOMER_NAME,
    Item : {
      messenger_id : {
        S: customer.messenger_id
      },
      last_name : {
        S: customer.last_name
      },
      gender : {
        S: customer.gender
      },
      telephone : {
        S: ""
      },
      balance : {
        N: 0
      },
      bot_feedback : {
        L: []
      },
      tip_feedback : {
        L: []
      }
    },
    ConditionExpression: 'attribute_not_exists (messenger_id)'
  }, function(err, data){
    if(err) {
      fn(JSON.stringify({
      "codeReturn" : -1,
      "message" : "Error during insertion in database",
      "messenger_id" : "0"
    }));
  }
    else{
      fn(JSON.stringify({
        "codeReturn" : 1,
        "message" : "Successful",
        "messenger_id" : customer.messenger_id
      }));
    }
  });
},

/**
** Update balance of customer id
** @customerid : The customer id
** @balance : The balance to add or substract to the actual balance
** @flag : 0 - add , 1 - substract
**/

updateBalance : (customerid, balance, flag,fn) => {
  if(flag == 0)
  {
    var expression = "set #balance = #balance + :balance";
  }else {
    var expression = "set #balance = #balance - :balance";
  }
  var params = {
    TableName : config.CUSTOMER_NAME,
    Key : {
      messenger_id : {
        S : customerid
      }
    },
    UpdateExpression : expression,
    ExpressionAttributes : {
      "#balance" : "balance"
    },
    ExpressionAttributeValues : {
      ":balance" : balance
    }
  }

  dynamodb.updateItem(params, function(err, data){
    if (err){
      fn(JSON.stringify({
        "codeReturn" : "0",
        "message" : "Couldn't update customer balance"
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Successful balance update",
      }));
    }
  });

},

/**
** Add bot feedback from a given customer
** @customerid : the customer's id of the customer
** @feedback : the feedback given by the customer
** @score : the score given by the customer
**/

addFeedBack : (customerid, feedback, score,fn) => {
  const feed = {
    "comment" : feedback,
    "score" : score,
    "date" : new Date()
  }
  var params = {
    TableName : config.CUSTOMER_NAME,
    Key : {
      messenger_id : {
        S: customerid
      }
    },
    UpdateExpression : "set #bot_feedback = list_append(#bot_feedback,:feed)",
    ExpressionAttributes : {
      "#bot_feedback" : "bot_feedback"
    },
    ExpressionAttributeValues : {
      ":feed" : feed
    }
  }

  dynamodb.updateItem(params, function(err, data){
    if (err){
      fn(JSON.stringify({
        "codeReturn" : "0",
        "message" : "Couldn't add customer feedback"
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Feedback successfully add",
      }));
    }
  });

},

/**
** Add tip feedback from a customer
** @customerid : the customer's id
** @tip_id : the id of the tip
** @feedback : -1: not useful, 0: neutral, 1: useful
**/

addTipFeedback : (customerid, tip_id, feedback,fn) => {

  const feed = {
    "date" : new Date(),
    "review" : feedback,
    "tip_id" : tip_id
  }

  var params = {
    TableName : config.CUSTOMER_NAME,
    Key : {
      messenger_id : {
        S: customerid
      }
    },
    UpdateExpression : "set #tip_feedback = list_append(#tip_feedback,:feed)",
    ExpressionAttributes : {
      "#tip_feedback" : "tip_feedback"
    },
    ExpressionAttributeValues : {
      ":feed" : feed
    }
  }

  dynamodb.updateItem(params, function(err, data){
    if (err){
      fn(JSON.stringify({
        "codeReturn" : "0",
        "message" : "Couldn't add customer feedback"
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Feedback successfully add",
      }));
    }
  });

},

/**
** Get customer info for graphql
** @customerid : the customer's id info we wish to have
**/

getInfo : (customerid,fn) => {
  var params = {
    TableName : config.CUSTOMER_NAME,
    Key : {
      messenger_id : {
        S : customerid
      }
    }
  }

  dynamodb.getItem(params, function(err, data){
    if (err) {
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Error during retrieval"
      }));
    }else {
      if('Item' in data){
        fn(JSON.stringify({
            "codeReturn" : "1",
            "customer_id" : data.Item.messenger_id.S,
            "message" : "Retrieval done",
            "id" : data.Item.messenger_id.S,
            "last_name" : data.Item.last_name.S,
            "gender" : data.Item.gender.S,
            "telephone" : data.Item.telephone.S,
            "balance" : data.Item.balance.S
        }));
      }else{
        fn(JSON.stringify({
          "codeReturn" : "-1",
          "message" : "No user found"
        }));
      }
    }
  });
},

/**
** Get feedbacks for customer id
** @customerid : the customer's id
**/

getFeedback : (customerid,fn) => {
  var params = {
    TableName : config.CUSTOMER_NAME,
    Key : {
      messenger_id : {
        S : customerid
      }
    }
  }

  dynamodb.getItem(params, function(err, data){
    if (err) {
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Error during retrieval"
      }));
    }else {
      if('Item' in data){

        var feedbacks = data.Item.bot_feedback.L
        var arrayFeed = []
        var customer = JSON.stringify({
            "id" : data.Item.messenger_id.S,
            "last_name" : data.Item.last_name.S,
            "gender" : data.Item.gender.S,
            "telephone" : data.Item.telephone.S,
            "balance" : data.Item.balance.N
        });

        var i = 0
        for(feedback in feedbacks)
        {
            arrayFeed.push({
              "customer" : customer,
              "rating" : feedback.score.N,
              "date" : feedback.date.S,
              "comment" : feedback.comment.S
            });
            i++
        }

        fn(JSON.stringify({
          "codeReturn" : "1",
          "feedbacks" : arrayFeed
        }))

      }else{
        fn(JSON.stringify({
          "codeReturn" : "-1",
          "message" : "No user found"
        }));
      }
    }
  });
},

getAllTipReviewsById : (tipid,fn) => {
  var params = {
    TableName : config.CUSTOMER_NAME,
    ProjectionExpression : "tip_feedback"
  }
  console.log("Getting the reviews")
  docuClient.scan(params, function(err, data){
    if (err) {
      console.log("Error"+err)
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Error during retrieval"
      }));
    }else{
      console.log("ok retrieving the data")
        var reviews = [];

        data.Items.forEach(function(element,index,array){
          //Treat each customer feedback
          for (feedback in element.tip_feedback)
          {
            if(feedback.tip_id == tipid)
            {
              reviews.push({"review ": feedback.review});
              break;
            }
          }
        });

        fn(JSON.stringify({
          "codeReturn" : "1",
          "reviews" : reviews
        }));

    }
  });
},

/**
** Get feedbacks for customer id
** @customerid : the customer's id
** @number : the number of feedbacks
**/

getFeebackNumber : (customerid, number,fn) => {
  var params = {
    TableName : config.CUSTOMER_NAME,
    Key : {
      messenger_id : {
        S : customerid
      }
    }
  }

  dynamodb.getItem(params, function(err, data){
    if (err) {
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Error during retrieval"
      }));
    }else {
      if('Item' in data){

        var feedbacks = data.Item.bot_feedback.L
        var arrayFeed = []
        var customer = JSON.stringify({
            "id" : data.Item.messenger_id.S,
            "last_name" : data.Item.last_name.S,
            "gender" : data.Item.gender.S,
            "telephone" : data.Item.telephone.S,
            "balance" : data.Item.balance.N
        });

        var i = 0
        for(feedback in feedbacks && i<number)
        {
            arrayFeed.push({
              "customer" : customer,
              "rating" : feedback.score.N,
              "date" : feedback.date.S,
              "comment" : feedback.comment.S
            });
            i++
        }

        fn(JSON.stringify({
            "feeds" : arrayFeed,
            "codeReturn" : "1"
        }));

      }else{
        fn(JSON.stringify({
          "codeReturn" : "-1",
          "message" : "No user found"
        }));
      }
    }
  });
},

/**
** Get all customers messengers id list
**/

getAllIds : (fn) => {
  var params = {
    TableName : config.CUSTOMER_NAME,
    ProjectionExpression : "messenger_id"
  }

  docuClient.scan(params, function(err,data){
    if(err){
      console.log(err)
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Error during retrieval"
      }));
    }else{

      var list = []
      data.Items.forEach(function(element,index,array){
          list.push(element.messenger_id)
      });

      fn(JSON.stringify({
        "list":list,
         "codeReturn":"1"
       }));
    }

  });
}

}

const AWS = require('aws-sdk')
const config = require('../../config.json')

const configOne = new AWS.Config({
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
    region : config.aws_region
});
const fs = require('fs')
AWS.config.update(configOne)

const crypto = require('crypto')
const PDFParser = require('pdf2json')
const pdfParser = new PDFParser()
const util = require('util')
const uuid = require('node-uuid')
const S3 = new AWS.S3()
const dynamodb = new AWS.DynamoDB()
const distance = require('google-distance')
const pageCount = require('pdf_page_count')
const PrinterService = require('./PrinterService')
const request = require('request')
const alphabet = "abcdefghijklmnopqrstuvwxyz"
const docuClient = new AWS.DynamoDB.DocumentClient()
const nodemailer = require('nodemailer2')
const EmailTemplate = require('email-templates-v2').EmailTemplate

distance.apiKey = config.GOOGLE_DISTANCE_API_KEY



/**
** Function to validate range
**/

function validateRange(pages_to_print, doclen,fn){
  var errorbit = 0;

  pages.to_print.split(",").forEach(function(val){
    var range = val.split("-");

    if(range.length > 2){
      errobit = 1;
    }
    if(range.length == 1 && range[0]!="end" && range[0].contains(alphabet)){
        errobit = 1
    }

    if(Number(range[0])>doclen || Number(range[1])>doclen || (Number(range[0])>Number(range[1]))) {
      errorbit = 1;
    }
  });

  if(errorbit != 1){
    fn(JSON.stringify({"verdict":"good"}));
  }else{
    fn(JSON.stringify({"verdict":"bad"}));
  }
}

/**
** Function to obtain the number of pages
** @string : pages_to_print
** @doclength : length of the document
**/

function nbpages(pages_to_print,doclength,fn){
  var nb = 0
  var pages = pages_to_print.split(",").forEach(function(val){
    var range = val.split("-");

    if(range.length == 1){
      nb ++;
    }
    else if(Number(range[1])>Number(doclength) || range[1]=="end"){
      range[1] = doclength
    }

    nb = Number(nb) + (Number(range[1]) - Number(range[0]));

  });

  fn(JSON.Stringify({"nb":nb}));

}

function countPages(url,fn){
  /*const fileName = uuid.v1()+".pdf"
  pdfParser.on('pdfParser_dataReady', function(data) {
    // var doc = data.PDFJS && data.PDFJS.pdfDocument && data.PDFJS.pdfDocument.numPages;
    return data.formImage.Pages.length;
  });
  fileStream = fs.createWriteStream(fileName)
  request(url).pipe(fileStream).on('finish',function(){
    pdfParser.loadPDF(fileName)
  })*/

  //Demo length = 10 pages

  fn("10");
  
}

/**
** Compute price compute price of a command for a given printer
** @prices : the printer's Prices
** @printloc : the printer's location
** @command : the command parameters
** @location : the user location
**/

function computePrice(prices, command, printloc, location,fn){
  
  if(location == null)
  {
    console.log("Ok, let me enter here")
          var doclen = 0
          var onepageprice = 0
          var priceExplain = "\n"
          //@TODO : Do the pageCount with request | pipe
          countPages(command.url,function(dat){
                    doclen = dat
                    console.log("compute Demo : doclen"+dat)
                    if(command.pages_to_print != "all")
                    {
                      doclen = nbpages(pages_to_print,doclen)
                    }
                    console.log("Nbpages doclen : "+doclen)
  
                    if(command.color == "Couleur" || command.color == "Color"){
                      if(command.paper_format == "A4"){
                        priceExplain += "Une page A4 couleur : "+prices.color_A4
                        onepageprice = prices.color_A4
                      }
                      if(command.paper_format == "A5"){
                        priceExplain += "Une page A5 couleur : "+prices.color_A5
                        onepageprice = prices.color_A5
                      }
                      if(command.paper_format == "A3"){
                        priceExplain += "Une page A3 couleur : "+prices.color_A3
                        onepageprice = prices.color_A3
                      }
                    }else{
                      if(command.paper_format == "A4"){
                        priceExplain += "Une page A4 noir/blanc : "+prices.gray_A4
                        onepageprice = prices.gray_A4
                      }
                      if(command.paper_format == "A5"){
                        priceExplain += "Une page A5 noir/blanc : "+prices.gray_A5
                        onepageprice = prices.gray_A5
                      }
                      if(command.paper_format == "A3"){
                        priceExplain += "Une page A3 noir/blanc : "+prices.gray_A3
                        onepageprice = prices.gray_A3
                      }
                    }
  
                    var price = onepageprice*doclen
                    priceExplain += "\n Pour "+doclen+" pages : "+onepageprice+"*"+doclen+""
                    console.log("onePagePrice : "+onepageprice+", doclen : +"+doclen+"Price : "+price)
  
                    if(command.rectoverso == "Yes" || command.rectoverso == "Oui"){
                      if(command.color == 1){
                          priceExplain += "\n Rectoverso : "+prices.color_rectoverso+"* "+doclen+" pages"
                          price += doclen*prices.color_rectoverso
                      }else{
                        priceExplain += "\n Rectoverso : "+prices.color_rectoverso+"* "+doclen+" pages"
                        price +=doclen*prices.gray_rectoverso
                      }
  
                    }
  
                    if(command.grouping!="Rien"){
                      if(command.grouping == "Reliure"){
                        priceExplain += "\n Reliure : "+prices.rayure 
                        price += prices.rayure
                      }
                      if(command.grouping == "Sers d'eau"){
                        priceExplain += "\n Sers d'eau : "+price.serseau 
                        price +=price.serseau
                      }
                      if(command.grouping == "Agraphe"){
                        priceExplain += "\n Agraphe : Gratuit"
                      }
  
                    }
                    console.log("Final price : "+price)
                    priceExplain += "\nPrix Final : "+price+"* "+command.copies+" copies = "
                    price = price*command.copies
                    priceExplain += ""+price
                    console.log("Final price Bis : "+price)
                    fn(JSON.stringify({
                      "codeReturn" : "1",
                      "price" : price,
                      "priceExplain" : priceExplain 
                    }))
          })
  }
  else
  {
    //If location is given by user
    //Process the distance between the printer and the user 
    console.log("Ok, let me enter here")

    distance.get({
      origin : location.latitude+","+location.longitude,
      destination : printloc.latitude+","+printloc.longitude,
    }, function(err, data){
      var doclen = 0
      var onepageprice = 0
      var priceExplain = "\n"
      //@TODO : Do the pageCount with request | pipe
      countPages(command.url,function(dat){
                doclen = dat
                console.log("compute Demo : doclen"+dat)
                if(command.pages_to_print != "all")
                {
                  doclen = nbpages(pages_to_print,doclen)
                }
                console.log("Nbpages doclen : "+doclen)

                if(command.color == "Couleur" || command.color == "Color"){
                  if(command.paper_format == "A4"){
                    priceExplain += "Une page A4 couleur : "+prices.color_A4
                    onepageprice = prices.color_A4
                  }
                  if(command.paper_format == "A5"){
                    priceExplain += "Une page A5 couleur : "+prices.color_A5
                    onepageprice = prices.color_A5
                  }
                  if(command.paper_format == "A3"){
                    priceExplain += "Une page A3 couleur : "+prices.color_A3
                    onepageprice = prices.color_A3
                  }
                }else{
                  if(command.paper_format == "A4"){
                    priceExplain += "Une page A4 noir/blanc : "+prices.gray_A4
                    onepageprice = prices.gray_A4
                  }
                  if(command.paper_format == "A5"){
                    priceExplain += "Une page A5 noir/blanc : "+prices.gray_A5
                    onepageprice = prices.gray_A5
                  }
                  if(command.paper_format == "A3"){
                    priceExplain += "Une page A3 noir/blanc : "+prices.gray_A3
                    onepageprice = prices.gray_A3
                  }
                }

                var price = onepageprice*doclen
                priceExplain += "\n Pour "+doclen+" pages : "+onepageprice+"*"+doclen+""
                console.log("onePagePrice : "+onepageprice+", doclen : +"+doclen+"Price : "+price)

                if(command.rectoverso == "Yes" || command.rectoverso == "Oui"){
                  if(command.color == 1){
                      priceExplain += "\n Rectoverso : "+prices.color_rectoverso+"* "+doclen+" pages"
                      price += doclen*prices.color_rectoverso
                  }else{
                    priceExplain += "\n Rectoverso : "+prices.color_rectoverso+"* "+doclen+" pages"
                    price +=doclen*prices.gray_rectoverso
                  }

                }

                if(command.grouping!="Rien"){
                  if(command.grouping == "Reliure"){
                    priceExplain += "\n Reliure : "+prices.rayure 
                    price += prices.rayure
                  }
                  if(command.grouping == "Sers d'eau"){
                    priceExplain += "\n Sers d'eau : "+price.serseau 
                    price +=price.serseau
                  }
                  if(command.grouping == "Agraphe"){
                    priceExplain += "\n Agraphe : Gratuit"
                  }

                }
                console.log("Final price : "+price)
                priceExplain += "\nPrix Final : "+price+"* "+command.copies+" copies = "
                price = Number(price)*command.copies
                priceExplain += "= "+price
                console.log("Final price Bis : "+price)
                fn(JSON.stringify({
                  "codeReturn" : "1",
                  "distance" : data,
                  "price" : price,
                  "priceExplain" : priceExplain 
                }))
      })
  })
  
}
}

/**
** Check if payment is completed
** @PostParams : the parameters of the payment url
**/

function checkPayment(PostParams,fn){

  var nbattempts = 100
  const delayBetweenRequest = 100;

  request(PostParams, function(error,response,body){
    if(!error && response.statusCode == 200){
      if(body.message == "payment pending"){
        nbattempts--
        if(!nbattempts) {
          fn(JSON.stringify({
            "codeReturn" : "0",
            "message" : "Couldn't obtain payment info"
          }));
        }
        setTimeout(checkPayment(PostParams),delayBetweenRequest)
      }else{
        fn(JSON.stringify({
          "codeReturn" : "1",
          "message" : "Successful"
        }))
      }
    }else{
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Error during payment checking"
      }))
    }
  });

}

module.exports = {

  /**
   * Compute price for the demo version (not taking into account the location of the user)
   * @command 
   * @returns the prices and currency 
   */

  computePriceDemo : (prices, command,fn) => {
   
          //Parse the doc to get the number of pages
          var doclen = 0
          var onepageprice = 0
          var priceExplain = "\n"
          //@TODO : Do the pageCount with request | pipe
          countPages(command.url,function(dat){
                    doclen = dat
                    console.log("compute Demo : doclen"+dat)
                    if(command.pages_to_print != "all")
                    {
                      doclen = nbpages(pages_to_print,doclen)
                    }
                    console.log("Nbpages doclen : "+doclen)
  
                    if(command.color == "Couleur" || command.color == "Color"){
                      if(command.paper_format == "A4"){
                        priceExplain += "Une page A4 couleur : "+prices.color_A4
                        onepageprice = prices.color_A4
                      }
                      if(command.paper_format == "A5"){
                        priceExplain += "Une page A5 couleur : "+prices.color_A5
                        onepageprice = prices.color_A5
                      }
                      if(command.paper_format == "A3"){
                        priceExplain += "Une page A3 couleur : "+prices.color_A3
                        onepageprice = prices.color_A3
                      }
                    }else{
                      if(command.paper_format == "A4"){
                        priceExplain += "Une page A4 noir/blanc : "+prices.gray_A4
                        onepageprice = prices.gray_A4
                      }
                      if(command.paper_format == "A5"){
                        priceExplain += "Une page A5 noir/blanc : "+prices.gray_A5
                        onepageprice = prices.gray_A5
                      }
                      if(command.paper_format == "A3"){
                        priceExplain += "Une page A3 noir/blanc : "+prices.gray_A3
                        onepageprice = prices.gray_A3
                      }
                    }
  
                    var price = onepageprice*doclen
                    priceExplain += "\n Pour "+doclen+" pages : "+onepageprice+"*"+doclen+""
                    console.log("onePagePrice : "+onepageprice+", doclen : +"+doclen+"Price : "+price)
  
                    if(command.rectoverso == "Yes" || command.rectoverso == "Oui"){
                      if(command.color == 1){
                          priceExplain += "\n Rectoverso : "+prices.color_rectoverso+"* "+doclen+" pages"
                          price += doclen*prices.color_rectoverso
                      }else{
                        priceExplain += "\n Rectoverso : "+prices.color_rectoverso+"* "+doclen+" pages"
                        price +=doclen*prices.gray_rectoverso
                      }
  
                    }
  
                    if(command.grouping!="Rien"){
                      if(command.grouping == "Reliure"){
                        priceExplain += "\n Reliure : "+prices.rayure 
                        price += prices.rayure
                      }
                      if(command.grouping == "Sers d'eau"){
                        priceExplain += "\n Sers d'eau : "+price.serseau 
                        price +=price.serseau
                      }
                      if(command.grouping == "Agraphe"){
                        priceExplain += "\n Agraphe : Gratuit"
                      }
  
                    }
                    console.log("Final price : "+price)
                    priceExplain += "\nPrix Final : "+price+"* "+command.copies+" copies = "
                    price = Number(price)*command.copies
                    priceExplain += "= "+price
                    console.log("Final price Bis : "+price)
                    fn(JSON.stringify({
                      "codeReturn" : "1",
                      "price" : price,
                      "priceExplain" : priceExplain 
                    }))
          })
   
  },
/**
** Function to save a command simply withouth a printer
** @color : the color of the printing
** @copies : the number of copies for the document
** @customer_id : the id of the customer
** @file_url : the url of the file to print
** @pages_to_print : the pages to print
** @paper_format : the paper format type
** @grouping : the type of grouping
** @rectoverso : the rectoverso mode
**/

  save : (color,copies,customer_id,file_url,pages_to_print,paper_format,grouping,rectoverso,name, fn) => {
    console.log("Saving initialisation")
      command_id = uuid.v1()

      var params = {
        TableName : config.COMMANDS_NAME,
        Item : {
          "color" : color,
          "copies" : copies,
          "customer_id" : customer_id,
          "file_url" : file_url,
          "pages_to_print" : pages_to_print,
          "paper_format" : paper_format,
          "command_id" : command_id,
          "printer_id" : "0000",
          "state" : "-1",
          "grouping" : grouping,
          "sent_time" : "0000",
          "retrieve_time" : "0000",
          "retrieved_time" : "0000",
          "name" : name,
          "achieved_time" : "0000",
          "chat" : [],
          "income" : "0",
          "fee" : "0",
          "recto-verso" : rectoverso
        }
      }

      docuClient.put(params, function(err, data){
        if(err) {
          console.log("Error during processing "+err)
          fn(JSON.stringify({
            "codeReturn" : "-1",
            "message" : "command couldn't be created"
          }));
        }else{
          fn(JSON.stringify({
            "command_id" : command_id,
            "codeReturn" : "1"
          }));
        }
      });
  },
  /**
  ** createOrder
  ** @customer_id : The customer concerned by the order 
  ** @command_id : the first command of the order
  ** @income : the income of the first command 
      For each command we will update the income to stay updated
  ** Returns the id of the order created which can later be used to add new commands to the order 
  **/
  createOrder : (customer_id,command_id,income,fn) => {
    id_order = uuid.v1()

    var params = {
      TableName : config.ORDER_NAME,
      Item : {
        "id_order" : id_order,
        "commands" : [command_id],
        "retrieved_time" : "0000",
        "achieved_time" : "0000",
        "customer_id" : customer_id,
        "chat" : [],
        "state" : "-1",
        "income" : parseFloat(income),
        "sent_time" : "0000",
        "printer_id" : "0000"
      }
    }

    docuClient.put(params,function(err,data){
      if(err){
        console.log("Error during order creation "+err)
        fn(JSON.stringify({
          "codeReturn" : "-1",
          "message" : "Order couldn't be created"
        }))
      }else{
        fn(JSON.stringify({
          "order_id" : id_order,
          "id_order" : id_order,
          "id" : id_order,
          "message" : "Well created",
          "codeReturn" : "1" 
        }))
      }
    })
  },
  /**
  **  addCommandToOrder
  **  @id_order : the id of the order concerned
  **  
  **/
  addCommandToOrder : (id_order,command_id,income,fn)=>{

    var newCommand = [command_id]
    var params = {
      TableName : config.ORDER_NAME,
      Key : {
        "id_order" : id_order
      },
      UpdateExpression : "set #commands = list_append(#commands,:newcommand), #income = #income + :newincome",
      ExpressionAttributeNames : {
        "#commands" : "commands",
        "#income" : "income"
      },
      ExpressionAttributeValues : {
        ":newcommand" : newCommand,
        ":newincome" : parseFloat(income)
      }

    }

    console.log("Income = "+parseFloat(income))

    docuClient.update(params,function(err,data){
      if(err){
        console.log("Error during the update of the command"+err)
        fn(JSON.stringify({
          "codeReturn" : "-1",
          "message" : "command couldn't be added to order"
        }))
      }else{
        fn(JSON.stringify({
          "codeReturn" : "1",
          "message" : "Well added",
          "id_order" : id_order
        }))
      }
    })    

  },
  /**
  * Send email of the printing order to the printer
  * @command_id : the id of the command 
  * @printer_mail : the mail of the printer 
  * @details : details about the command being implemented 
  **/
  sendEmail : (command_id,printer_mail,details,fn) =>{

    var transporter = nodemailer.createTransport({
      service : 'gmail',
      auth : {
        user : config.everyprint_mail,
        pass : config.everyprint_pass
      }
    });

    var email = transporter.templateSender(new EmailTemplate('./EmailSent'),{from: config.everyprint_mail});
    email({
      to : printer_mail,
      subject : 'PRINTING ORDER '+command_id
    },{
      messenger_id : details.messenger_id,
      command_id : details.command_id,
      paid : details.paid,
      color : details.color, 
      copies : details.copies,
      range : details.pages_to_print,
      grouping : details.grouping,
      paper : details.paper_format,
      senttime : details.sent, 
      balance : details.balance,
      name : details.name,
      file_url : details.url,
      rectoverso : details.rectoverso,
      fbprofile : details.name 
    }, function(err,info){
      if(err){
        console.log("Error during email sending .....\n Err : "+err+"\nInfo: "+info)
        fn(err,null)
      } else {
        console.log("Email envoyé avec succes .....")
        fn(null, "good")
      }
    });

  },

 /**
 ** Compute Command for a list of printer to give actual price
 ** @commandid : the id of the command
 ** @location : the location of the the user
 **/
  computeCommand : (command_id,location,fn) => {
    PrinterService.getPrintersIds(5,function(Printers){
          Printers = JSON.parse(Printers)
          if(Printers.codeReturn == "-1") {
            return JSON.stringify({
              "codeReturn" : "-1",
              "message": "Couldn't retrieve printers list"
            });
          }

          var elements = []
          console.log("Printer's list retrieved "+Printers.list)
          Printers.list.forEach(function(printer){
            console.log(printer.prices+"=====")
            console.log(command_id+"====")
            module.exports.getCommandById(command_id, function(com){
              com = JSON.parse(com)
              console.log("Command = " +com.command_id)
              computePrice(printer.prices,com,printer.location,location,function(thePrice){
                thePrice = JSON.parse(thePrice)
                if(location == null)
                {
                  var jsondata = {
                    "title" : printer.name,
                    "image_url" : printer.logo_url,
                    "subtitle" : thePrice.price+config.currency+"\nDetails:\n"+thePrice.priceExplain,
                    "buttons" : [
                      {
                        "type" : "web_url",
                        "url" : "api/to/pay&command_id="+command_id+
                                  "&printer_id="+printer.printer_id+
                                  "&price="+thePrice.price+
                                  "&customer_id="+com.customer_id,
                        "title" : "Payer ici !!!"
                      }
                    ]
                  }
                }else{
                  var jsondata = {
                    "title" : printer.name,
                    "image_url" : printer.logo,
                    "subtitle" : thePrice.price+config.currency+
                                "\n At "+thePrice.data.distance+"from you\n "+
                                "About "+thePrice.data.duration+"\nDetails:\n"+thePrice.priceExplain,
                    "buttons" : [
                      {
                        "type" : "web_url",
                        "url" : "api/to/pay&command_id="+command_id+
                                  "&printer_id="+printer.printer_id+
                                  "&price="+thePrice.price+
                                  "&customer_id="+com.customer_id,
                        "title" : "Pay here !"
                      },
                      {
                        "type" : "web_url",
                        "url" : config.mapsGoogleUrl+
                                "saddr="+location.latitude+","+location.longitude+
                                "daddr="+printer.location.latitude+","+printer.location.longitude,
                        "title" : "View the road"
                      }
                    ]
                  }
                }
                console.log("jsondata" + jsondata)
                elements.push(jsondata);

                var messages = {
                  "messages" : [
                    {
                      "text" : "Here are nearest printers for your command :)"
                    },
                    {
                        "attachment" : {
                          "type" : "template",
                          "payload" : {
                            "template_type" : "generic",
                            "image_aspect_ratio" : "square",
                            "elements" : elements
                          }
                        }
                    }
                  ]
                };
      
                fn(JSON.stringify({
                  "codeReturn" : "1",
                  "messages" : messages
                }));
              })

            });


          });
         



    });

  },

/**
** Accept the command details and retrieve the money from the
** customer's account
** @command_id : the command id
** @printer_id : the id of the printer to whom send the command
** @phone_number : the phone number of the customer
**/

processPayment : (command_id, printer_id,phone_number,amount,fn) => {
  //[@TODO: Verify if the amount is the good to pay]

  var PaymentParams = {
    "service" : config.MONET_BIL_SERVICE_KEY,
    "phonenumber" : phone_number,
    "amount" : amount
  }

  var PostParams =  {
    "url" : config.place_payment_url,
    "method" : "POST",
    "headers" : {'Content-Type' : 'application/json'},
    "qs" : PaymentParams
  }

  request(PostParams, function(error,response,body){
    if (!error && response.statusCode == 200) {

      if(body.status == "REQUEST_ACCEPTED")
      {
          var paymentId = body.paymentId
          PaymentParams = {
            "paymentId" : paymentId
          }

          PostParams = {
            "url" : config.check_payment_url,
            "method" : "POST",
            "qs" : PaymentParams
          }

          checkPayment(PostParams,function(paymentResult){
            paymentResult = JSON.parse(paymentResult)
            if(paymentResult.codeReturn == "0"){
              fn(JSON.stringify({
                "codeReturn" : "0",
                "message" : "Couldn't verify transaction"
              }));


            }else if(PaymentResult.codeReturn == "-1"){
              fn(JSON.stringify({
                "codeReturn" : "-1",
                "message" : "Error during payment processing"
              }));
            }else{
              //Transaction done
              //Update every statistics to update

              var dateBis = new Date(Date.now()).toLocaleString()
              this.changeState(command_id,"0", function(dataState){
                dataState = JSON.parse(dataState)
                if(dataState.codeReturn == "1"){
                    this.changeFee(command_id,JSON.paymentResult.transaction.fee, function(dataFee){
                      dataFee = JSON.parse(dataFee)
                      if(dataFee.codeReturn == "1"){
                        this.changeIncome(command_id,amount, function(dataIncome){
                          dataIncome = JSON.parse(dataIncome)
                          if(dataIncome.codeReturn == "1"){
                            this.changeSentTime(command_id,dateBis, function(datasent){
                              dataSent = JSON.parse(datasent)
                              if(datasent.codeReturn == "1"){
                                this.changePrinterId(command_id, printer_id, function(dataPrint){
                                  dataPrint = JSON.parse(dataPrint)
                                  if(dataPrint.codeReturn == "1"){
                                    PrinterService.updateBalance(printer_id,amount,0,function(result){
                                      result = JSON.parse(result)
                                      if(result.codeReturn == "1"){
                                        PrinterService.updatePerDay(printer_id,dateBis,amount, function(resultUp){
                                            resultUp = JSON.parse(resultUp)
                                          if(resultUp.codeReturn == "1"){

                                                          fn(JSON.stringify({
                                                            "codeReturn" : "1",
                                                            "messages" : [
                                                                {"text":"Command sent successfully. We will notify you when command is done"}
                                                            ]
                                                          }));
                                          }
                                        })
                                      }
                                    })
                                  }
                                })
                              }
                            })

                          }
                        })
                      }
                    })
                }
              })

      }
    })


   }else{
     fn(JSON.stringify({
       "codeReturn":"-1",
       "message" : "Couldn't place payment"
     }))
   }
 }});

},/**
** Change order state
** @order_id : the id of the order to update
** @state : 0 paid, 1 finished, 2 retrieved
**/
changeOrderState : (order_id, state,fn) => {
  var params = {
    TableName : config.ORDER_NAME,
    Key : {
      "id_order" : order_id
    },
    UpdateExpression : "set #state = :s",
    ExpressionAttributeNames : {
      "#state" : "state"
    },
    ExpressionAttributeValues : {
      ":s" : state
    }
  }

  docuClient.update(params, function(err,data){
    if(err) {
      console.log("state : "+state+", order_id:"+order_id)
      console.log("State couldn't be updated , "+err)
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't update item"
      }));
    }else{
      console.log("State updated successfully")
      if(state == "1"){
        module.exports.updateOrderAchievedTime(order_id,new Date(Date.now()).toLocaleString(),function(updated){
          up = JSON.parse(updated)
          console.log("Update achieved time "+up.codeReturn)
          if(up.codeReturn == "1"){
            module.exports.getOrderById(order_id,function(command){
              console.log("Command Retrieved successuful \n Customer id : "+JSON.parse(command).customer_id)
              var params = {
                url : "https://api.chatfuel.com/bots/"
                      + config.EVEYPRINT_BOT_ID +"/users/"
                      + JSON.parse(command).customer_id + "/send",
                method : 'POST',
                qs : {
                  "chatfuel_token" : config.CHATFUEL_API_KEY,
                  "chatfuel_block_name" : config.EVERYPRINT_COMMAND_OK,
                  "printer_name" : name,
                  "command_id" : order_id,
                }
              }

              request(params, function(error, response, body){
                if(error){
                  fn(JSON.stringify({
                    "codeReturn" : "0",
                    "message" : "Error during post",
                    "body" : body
                  }));
                }else{
                  fn(JSON.stringify({
                    "codeReturn" : "1",
                    "message" : "Update successful",
                    "body" : body
                  }));
                }
              })
            });
          }
        })
        //Notify corresponding user

      }

      if(state == "2"){
        module.exports.updateOrderRetrievedTime(order_id,new Date(Date.now()).toLocaleString(),function(retrieved){
          console.log("Update retrieved time")

          if(JSON.parse(retrieved).codeReturn == "1"){
            module.exports.getCommandById(command_id,function(com){
              command = JSON.parse(com)
              var params = {
                url : "https://api.chatfuel.com/bots/"
                      + config.EVEYPRINT_BOT_ID +"/users/"
                      + command.customer_id + "/send",
                method : 'POST',
                qs : {
                  "chatfuel_token" : config.CHATFUEL_API_KEY,
                  "chatfuel_block_name" : config.COMMAND_RETRIEVAL_BLOCK,
                  "name_retrieved" : command.name,
                  "command_id_retrieved" : order_id
                }
              }

              request(params, function(error, response, body){
                if(error){
                  fn(JSON.stringify({
                    "codeReturn" : "0",
                    "message" : "Error during post",
                    "body" : body
                  }));
                }else{
                  fn(JSON.stringify({
                    "codeReturn" : "1",
                    "message" : "Update successful",
                    "body" : body
                  }));
                }
              })
            })
        }
      })
      }

      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Well updated",
        "body" : "Fine"
      }));
    }
  });
},

/**
** Change command state
** @command_id : the id of the command to update
** @state : 0 paid, 1 finished, 2 retrieved
**/
changeState : (command_id, state,fn) => {
  var params = {
    TableName : config.COMMANDS_NAME,
    Key : {
      "command_id" : command_id
    },
    UpdateExpression : "set #state = :s",
    ExpressionAttributeNames : {
      "#state" : "state"
    },
    ExpressionAttributeValues : {
      ":s" : state
    }
  }

  docuClient.update(params, function(err,data){
    if(err) {
      console.log("state : "+state+", command_id:"+command_id)
      console.log("State couldn't be updated , "+err)
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't update item"
      }));
    }else{
      console.log("State updated successfully")
      if(state == "1"){
        module.exports.updateAchievedTime(command_id,new Date(Date.now()).toLocaleString(),function(updated){
          up = JSON.parse(updated)
          console.log("Update achieved time "+up.codeReturn)
          if(up.codeReturn == "1"){
            module.exports.getCommandById(command_id,function(command){
              console.log("Command Retrieved successuful \n Customer id : "+JSON.parse(command).customer_id)
              var params = {
                url : "https://api.chatfuel.com/bots/"
                      + config.EVEYPRINT_BOT_ID +"/users/"
                      + JSON.parse(command).customer_id + "/send",
                method : 'POST',
                qs : {
                  "chatfuel_token" : config.CHATFUEL_API_KEY,
                  "chatfuel_block_name" : config.EVERYPRINT_COMMAND_OK,
                  "printer_name" : name,
                  "command_id" : command_id
                }
              }

              request(params, function(error, response, body){
                if(error){
                  fn(JSON.stringify({
                    "codeReturn" : "0",
                    "message" : "Error during post",
                    "body" : body
                  }));
                }else{
                  fn(JSON.stringify({
                    "codeReturn" : "1",
                    "message" : "Update successful",
                    "body" : body
                  }));
                }
              })
            });
          }
        })
        //Notify corresponding user

      }

      if(state == "2"){
        module.exports.updateRetrievedTime(command_id,new Date(Date.now()).toLocaleString(),function(retrieved){
          console.log("Update retrieved time")

          if(JSON.parse(retrieved).codeReturn == "1"){
            module.exports.getCommandById(command_id,function(com){
              command = JSON.parse(com)
              var params = {
                url : "https://api.chatfuel.com/bots/"
                      + config.EVEYPRINT_BOT_ID +"/users/"
                      + command.customer_id + "/send",
                method : 'POST',
                qs : {
                  "chatfuel_token" : config.CHATFUEL_API_KEY,
                  "chatfuel_block_name" : config.COMMAND_RETRIEVAL_BLOCK,
                  "name_retrieved" : command.name,
                  "command_id_retrieved" : command_id
                }
              }

              request(params, function(error, response, body){
                if(error){
                  fn(JSON.stringify({
                    "codeReturn" : "0",
                    "message" : "Error during post",
                    "body" : body
                  }));
                }else{
                  fn(JSON.stringify({
                    "codeReturn" : "1",
                    "message" : "Update successful",
                    "body" : body
                  }));
                }
              })
            })
        }
      })
      }

      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Well updated",
        "body" : "Fine"
      }));
    }
  });
},

updateRetrievedTime : (command_id,date,fn) => {
  var params = {
    TableName : config.COMMANDS_NAME,
    Key : {
      "command_id" : command_id
    },
    UpdateExpression : "set retrieved_time = :date",
    ExpressionAttributeValues : {
      ":date" : date
    }
  }

  docuClient.update(params, function(err,data){
    if(err) {
    console.log("Couldn't update retrieved time ")
    fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't update item"
      }));
    }else{
      console.log("Updated retrieved time successfully")
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Update successful"
      }));
    }
  });
},
updateOrderRetrievedTime : (order_id,date,fn) => {
  var params = {
    TableName : config.ORDER_NAME,
    Key : {
      "id_order" :order_id
    },
    UpdateExpression : "set retrieved_time = :date",
    ExpressionAttributeValues : {
      ":date" : date
    }
  }

  docuClient.update(params, function(err,data){
    if(err) {
    console.log("Couldn't update retrieved time ")
    fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't update item"
      }));
    }else{
      console.log("Updated retrieved time successfully")
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Update successful"
      }));
    }
  });
},

/**
** Update the achieved time of the command
** @command_id : the id of the command
** @date : the date at which the command is done
**/

updateAchievedTime : (command_id, date, fn) => {
  console.log("Entered updatedAchievedTime")
  var params = {
    TableName : config.COMMANDS_NAME,
    Key : {
      "command_id" : command_id
    },
    UpdateExpression : "set achieved_time = :date",
    ExpressionAttributeValues : {
      ":date" : date
    }
  }

  docuClient.update(params, function(err,data){
    if(err) {
      console.log('Badly updated')
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't update item"
      }));
    }else{
      console.log("Updated achieved time successfully")
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Update successful"
      }));
    }
  });
},

updateOrderAchievedTime : (order_id, date, fn) => {
  console.log("Entered updatedAchievedTime")
  var params = {
    TableName : config.ORDER_NAME,
    Key : {
      "id_order" : order_id
    },
    UpdateExpression : "set achieved_time = :date",
    ExpressionAttributeValues : {
      ":date" : date
    }
  }

  docuClient.update(params, function(err,data){
    if(err) {
      console.log('Badly updated')
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't update item"
      }));
    }else{
      console.log("Updated achieved time successfully")
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Update successful"
      }));
    }
  });
},

/**
** Change command fee
** @command_id : the id of the command to update
** @fee : the fee of the command
**/
changeFee : (command_id, fee,fn) => {
  var params = {
    TableName : config.COMMANDS_NAME,
    Key : {
      "command_id" : command_id
    },
    UpdateExpression : "set fee = :fee",
    ExpressionAttributeValues : {
      ":fee" : fee
    }
  }

  docuClient.update(params, function(err,data){
    if(err) {
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't update item"
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Update successful"
      }));
    }
  });
},


/**
** Change command income
** @command_id : the id of the command to update
** @income : the income of a command
**/
changeIncome : (command_id, income,fn) => {
  var params = {
    TableName : config.COMMANDS_NAME,
    Key : {
      "command_id" : command_id
    },
    UpdateExpression : "set income = :income",
    ExpressionAttributeValues : {
      ":income" : income
    }
  }

  docuClient.update(params, function(err,data){
    if(err) {
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't update item"
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Update successful"
      }));
    }
  });
},

/**
** Update sent time
** @command_id : the id of the command
** @sent_time : the sent_time to update
**/
changeSentTime : (command_id,sent_time,fn) => {
  var params = {
    TableName : config.COMMANDS_NAME,
    Key : {
      "command_id" : command_id
    },
    UpdateExpression : "set sent_time = :sent_time",
    ExpressionAttributeValues : {
      ":sent_time" : sent_time
    }
  }

  docuClient.update(params, function(err,data){
    if(err) {
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't update item"
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Update successful"
      }));
    }
  });
},
/**
** Update sent time order
** @order_id : the id of the order
** @sent_time : the sent_time to update
**/
changeOrderSentTime : (order_id,sent_time,fn) => {
  var params = {
    TableName : config.ORDER_NAME,
    Key : {
      "id_order" : order_id
    },
    UpdateExpression : "set sent_time = :sent_time",
    ExpressionAttributeValues : {
      ":sent_time" : sent_time
    }
  }

  docuClient.update(params, function(err,data){
    if(err) {
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't update item"
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Update successful"
      }));
    }
  });
},
/**
** Change printer id for a command
** @command_id : the id of the command
** @printer_id : the id of the printer
**/

changePrinterId : (command_id,printer_id,fn) => {
  var params = {
    TableName : config.COMMANDS_NAME,
    Key : {
      "command_id" : command_id
    },
    UpdateExpression : "set printer_id = :printer_id",
    ExpressionAttributeValues : {
      ":printer_id" : printer_id
    }
  }

  docuClient.update(params, function(err,data){
    if(err) {
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't update item"
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Update successful"
      }));
    }
  });
},
/**
** Change order printer id for a command
** @order_id : the id of the order
** @printer_id : the id of the printer
**/

changeOrderPrinterId : (order_id,printer_id,fn) => {
  var params = {
    TableName : config.ORDER_NAME,
    Key : {
      "id_order" : order_id
    },
    UpdateExpression : "set printer_id = :printer_id",
    ExpressionAttributeValues : {
      ":printer_id" : printer_id
    }
  }

  docuClient.update(params, function(err,data){
    if(err) {
      console.log(err)
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't update item"
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Update successful"
      }));
    }
  });
},

/**
** Get orders info by printer and state required
** @printer_id : the id of the printer
**/

getOrdersForPrinterAll : (printer_id,fn) => {
  var params = {
    TableName : config.ORDER_NAME,
    FilterExpression : "printer_id = :p",
    ExpressionAttributeValues : {
      ":p" : printer_id
    }

  }

  var orders = []
  //console.log("launching")

  docuClient.scan(params, function(err,data){
    if(err) {
      console.log("not pusshing\n"+err)
      return JSON.stringify({
        "codeReturn" : "-1",
        "message" : "couldn't retrieve the orders details"
      });
    }else{
      console.log("retrieved")
      data.Items.forEach(function(element,index,array){
        console.log("pusshing command")
        orders.push({
          "id_order" : element.id_order,
          "customer_id" : element.customer_id,
          "state" : element.state,
          "printer_id" : element.printer_id,
          "sent_time" : element.sent_time,
          "achieved_time" : element.achieved_time,
          "retrieved_time": element.retrieved_time,
          "income" : element.income,
          "commands" : element.commands
        });
      });
      console.log(orders)
      fn(JSON.stringify({
        "codeReturn" : "1",
        "orders" : orders
      }));
      }
  });
},


/**
** Get commands info by printer and state required
** @command_id : the command identifier
** @printer_id : the id of the printer
** @state : the state of the command willing
**/

getCommandsForPrinterAll : (printer_id,fn) => {
  var params = {
    TableName : config.COMMANDS_NAME,
    FilterExpression : "printer_id = :p",
    ExpressionAttributeValues : {
      ":p" : printer_id
    }

  }

  var commands = []
  console.log("launching")

  docuClient.scan(params, function(err,data){
    if(err) {
      console.log("not pusshing\n"+err)
      return JSON.stringify({
        "codeReturn" : "-1",
        "message" : "couldn't retrieve the commands details"
      });
    }else{
      console.log("retrieved")
      data.Items.forEach(function(element,index,array){
        console.log("pusshing command")
        commands.push({
          "command_id" : element.command_id,
          "color" : element.color,
          "copies" : element.copies,
          "file_url" : element.file_url,
          "pages_to_print" : element.pages_to_print,
          "customer_id" : element.customer_id,
          "paper_format" : element.paper_format,
          "grouping" : element.grouping,
          "state" : element.state,
          "printer_id" : element.printer_id,
          "sent_time" : element.sent_time,
          "achieved_time" : element.achieved_time,
          "retrieved_time": element.retrieved_time,
          "income" : element.income,
          "fee" : element.fee
        });
      });
      console.log(commands)
      fn(JSON.stringify({
        "codeReturn" : "1",
        "commands" : commands
      }));
      }
  });
},

/**
** Get orders info by printer and state required
** @printer_id : the id of the printer
** @state : the state of the command willing
**/

getOrdersForPrinterState : (printer_id,state,fn) => {
  var params = {
    TableName : config.ORDER_NAME,
    FilterExpression : "printer_id = :p and #state = :s",
    ExpressionAttributeNames : {
      "#state" : "state"
    },
    ExpressionAttributeValues : {
      ":p" : printer_id,
      ":s" : state
    }

  }

  var orders = []

  docuClient.scan(params, function(err,data){
    if(err) {
      console.log(err)
      return JSON.stringify({
        "codeReturn" : "-1",
        "message" : "couldn't retrieve the commands details"
      });
    }else{
      console.log("Entered here, fine for retrieving state")
      if(data.Items.length > 0){
        data.Items.forEach(function(element,index,array){
        console.log("good, pushing")
         orders.push({
            "id_order" : element.id_order,
            "customer_id" : element.customer_id,
            "state" : element.state,
            "printer_id" : element.printer_id,
            "sent_time" : element.sent_time,
            "achieved_time" : element.achieved_time,
            "retrieved_time": element.retrieved_time,
            "income" : element.income,
            "commands" : element.commands
          });
      });
      }else{
        orders = [null]
      }
      
      fn(JSON.stringify({
        "codeReturn" : "1",
        "orders" : orders
      }));
      }
  });
},

/**
** Get commands info by printer and state required
** @printer_id : the id of the printer
** @state : the state of the command willing
**/

getCommandsForPrinterState : (printer_id,state,fn) => {
  var params = {
    TableName : config.COMMANDS_NAME,
    FilterExpression : "printer_id = :p and #state = :s",
    ExpressionAttributeNames : {
      "#state" : "state"
    },
    ExpressionAttributeValues : {
      ":p" : printer_id,
      ":s" : state
    }

  }

  var commands = []

  docuClient.scan(params, function(err,data){
    if(err) {
      console.log(err)
      return JSON.stringify({
        "codeReturn" : "-1",
        "message" : "couldn't retrieve the commands details"
      });
    }else{
      console.log("Entered here, fine for retrieving state")
      data.Items.forEach(function(element,index,array){
        console.log("good, pushing")
        commands.push({
          "command_id" : element.command_id,
          "color" : element.color,
          "copies" : element.copies,
          "file_url" : element.file_url,
          "pages_to_print" : element.pages_to_print,
          "customer_id" : element.customer_id,
          "paper_format" : element.paper_format,
          "grouping" : element.grouping,
          "state" : element.state,
          "printer_id" : element.printer_id,
          "sent_time" : element.sent_time,
          "achieved_time" : element.achieved_time,
          "retrieved_time": element.retrieved_time  ,
          "fee" : element.fee,
          "income" : element.income
        });
      });
      console.log(commands)
      fn(JSON.stringify({
        "codeReturn" : "1",
        "commands" : commands
      }));
      }
  });
},

  /**
  ** Get a command details
  ** @commandid : the command id
  **/

getCommandById : (command_id,fn) => {
    var params = {
      TableName : config.COMMANDS_NAME,
      Key: {
        command_id : {
          S : command_id
        }
      } 
    }
    console.log("Trying to retrieve command for id "+command_id)
    dynamodb.getItem(params, function(err,data){
      if(err) {
        console.log("Err "+err)
        return JSON.stringify({
          "codeReturn" : "-1",
          "message" : "couldn't retrieve the commands details"
        });
      }else{
        console.log("Object found")
          if ('Item' in data){
            fn(JSON.stringify({
              "codeReturn" : "1",
              "color" : data.Item.color.S,
              "copies" : data.Item.copies.S,
              "customer_id" : data.Item.customer_id.S,
              "income" : data.Item.income.S,
              "fee" : data.Item.fee.S,
              "file_url" : data.Item.file_url.S,
              "pages_to_print" : data.Item.pages_to_print.S,
              "paper_format" : data.Item.paper_format.S,
              "command_id" : data.Item.command_id.S,
              "grouping" : data.Item.grouping.S,
              "name" : data.Item.name.S,
              "state" : data.Item.state.S,
              "sent_time" : data.Item.sent_time.S,
              "achieved_time" : data.Item.achieved_time.S
            }));
          }
          else{
            console.log("Not found")
            fn(JSON.stringify({
              "codeReturn" : "0",
              "message" : "No command corresponding"
            }))
          }
      }
    });
  },
  /**
  ** Get a order details
  ** @order_id : the order id
  **/

getOrderById : (order_id,fn) => {
    var params = {
      TableName : config.ORDER_NAME,
      Key: {
        id_order : {
          S : order_id
        }
      } 
    }
    console.log("Trying to retrieve command for id "+command_id)
    dynamodb.getItem(params, function(err,data){
      if(err) {
        console.log("Err "+err)
        return JSON.stringify({
          "codeReturn" : "-1",
          "message" : "couldn't retrieve the commands details"
        });
      }else{
        console.log("Object found")
          if ('Item' in data){
            fn(JSON.stringify({
              "codeReturn" : "1",
              "id_order" : order_id,
              "customer_id" : data.Item.customer_id.S,
              "state" : data.Item.state.S,
              "printer_id" : data.Item.printer_id.S,
              "sent_time" : data.Item.sent_time.S,
              "achieved_time" : data.Item.achieved_time.S,
              "retrieved_time": data.Item.retrieved_time.S,
              "income" : data.Item.income.N,
              "commands" : data.Item.commands.L
            }));
          }
          else{
            console.log("Not found")
            fn(JSON.stringify({
              "codeReturn" : "0",
              "message" : "No command corresponding"
            }))
          }
      }
    });
  }
}

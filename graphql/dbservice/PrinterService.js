const AWS = require('aws-sdk')
const config = require('../../config.json')

const configOne = new AWS.Config({
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
    region : config.aws_region
});

AWS.config.update(configOne);


const crypto = require('crypto')
const util = require('util')
const uuid = require('node-uuid')
const S3 = new AWS.S3({apiVersion: '2006-03-01'})
const dynamodb = new AWS.DynamoDB()
const ses = new AWS.SES()
const fs = require('fs')
const path = require('path')
const cognitoidentity = new AWS.CognitoIdentity()
const request = require('request')
const CustomerService = require('./CustomerService')
const nodemailer = require('nodemailer2')
const EmailTemplate = require('email-templates-v2').EmailTemplate
const fileType = require('file-type')
const docuClient = new AWS.DynamoDB.DocumentClient()
const apiBaseUrl = "https://9j8qx1wsl0.execute-api.us-east-1.amazonaws.com/dev/api/"





/**
** Returns the type of the fileName to send in parameter in chatfuel
** @fileName : the name of the parameter
**/

function getFileType(fileName)
{
  extension = fileName.split("/")[4].split(".")[1]


  if(["mp4","avi","webv"].includes(extension)) return "video"
  if(["mp3","m3","wav"].includes(extension)) return "audio"
  if(["pdf","docx","pptx","odt","epub"].includes(extension)) return "file"

  return "";
}

/**
** Computes a hash and salt for a given password
** @password : password given
** @salt : Generate the salt and outputs the hash
** @fn : a callback function
**/
function computeHash(password, salt, fn) {
  var len = config.CRYPTO_BYTE_SIZE;
  var iterations = 4096;

  if(3 == arguments.length)
  {
    crypto.pbkdf2(password,salt,iterations,len,'sha1',fn)
  }else {
    fn = salt
    crypto.randomBytes(len, function(err,salt){
      if (err) return fn(err)
      salt = salt.toString('base64')
      crypto.pbkdf2(password,salt,iterations,len, 'sha1',function(err,derivedkey){
        if (err) return fn(err)
        fn(null,salt,derivedkey.toString('base64'))
      });
    });
  }
}

/**
** Uploads a given file to AWS S3 Bucket for EveryPrint
** @file : The file given to uplad
** @fn : a callback function
**/
function uploadFileToS3(file,fn){

console.log("Calling me to upload "+file)

  const fileBuffer = new Buffer(file,'base64')
  console.log(fileBuffer.toString('ascii').length)
  const fileTypeInfo = fileType(fileBuffer)
  console.log(fileTypeInfo)
/*  if(fileTypeInfo.length > 500000){
    return JSON.stringify({
      "codeReturn" : "-1",
      "message" : "file too big"
    });
  }
*/

  var uploadParams = {Bucket:config.S3_BUCKET,Key:'',Body:''}
  var fileStream = fs.createReadStream(file)
  fileStream.on('error',function(err){
    return fn(err)
  });
  uploadParams.Body = fileBuffer
  uploadParams.Key = file

  S3.putObject(uploadParams, function(err, data){
      if(err){
        fn(err,null);
      }if(data){
        fn(null,data.Location);
      }
  });
}
/**
** A function sending a mail to the printer in order to activate
** his account
** @printer : printer info
** @token : The token issued to him by mail
** @fn : A callback function
**/
function sendVerificationEmail(printer, id, token,fn){
    const subject = 'ACCOUNT ACTIVATION FOR '+config.EXTERNAL_NAME

    var transporter = nodemailer.createTransport({
      service : 'gmail',
      auth : {
        user : config.everyprint_mail,
        pass : config.everyprint_pass
      }
    });

    var email = transporter.templateSender(new EmailTemplate('./Email'),{from : config.everyprint_mail});

      email({
        to : printer.mail,
        subject : 'ACCOUNT ACTIVATION FOR '+config.EXTERNAL_NAME,
      },{
        name : printer.name,
        url : config.VALIDATE_PRINTER_URL+"?printer_id="+id+"&token="+token
      },function(err,info){
        if (err) {
           fn(err,null)
       } else {
           fn(null,"good")
       }
     });

   }

module.exports = {

/**
** Upload logo for the printer
**/
//[@TODO]
uploadLogo : (printer_id, logo_url, fn) => {

},

/**
** Creates a printer in the dynamo table and issues token
** for verification
** @printer : json object containing printer information
** @salt : salt given by the function computeHash
** @fn : A callback function
**/

save : (printer,fn) => {
  //console.log(JSON.parse(printer.prices).color_A3)
    var len = config.CRYPTO_BYTE_SIZE;
    var default_prices = {
    "color_A3": 100,
    "color_A4": 15,
    "color_A5": 75,
    "color_rectoverso": 10,
    "gray_A3": 75,
    "gray_A4": 10,
    "gray_A5": 50,
    "gray_rectoverso": 20,
    "rayure": 250,
    "serseau": 200
    }

    var location = {
      "latitude" : printer.location.split(",")[0],
      "longitude" : printer.location.split(",")[1]
    }
            var printer_id = uuid.v1()
            computeHash(printer.secret_key, function(err, salt, hash){
              crypto.randomBytes(len, function(err, token){
                  if (err) return fn(err);
                  token = token.toString('hex');
                  var params = {
                    TableName: config.PRINTERS_NAME,
                    Item : {
                      "printer_id":printer_id,
                      "nom" : printer.name,
                      "balance" : 0,
                      "location" : location,
                      "mail" : printer.mail,
                      "telephone" : printer.telephone,
                      "prices" : default_prices,
                      "logo_url" : config.def_logo,
                      "balance_history" : [],
                      "secret_key" : hash,
                      "bif_key" : hash,
                      "verified" : false,
                      "verifyToken" : token,
                      "available" : 0,
                      "passSalt" : salt
                    }
                  }

                  docuClient.put(params, function(err, data){
                      if (err) {console.log(err); return fn(err)}
                      else {
                        console.log(data)
                        //Send email token
                      sendVerificationEmail(printer,printer_id,token, function(err,data){
                        if(err){console.log(err); fn(JSON.stringify({
                          "codeReturn" : "1",
                          "printer_id" : printer_id,
                          "email" : printer.mail
                        }))}

                        else{
                          fn(JSON.stringify({
                            "codeReturn" : "1",
                            "printer_id" : printer_id,
                            "email" : printer.mail
                          }));
                        }
                      });
                      }
                  });
              });


      });

},



/**
** Function that gets activation data for printer's activation
** @email : the email of the printer to activate
** @fn : a callback function
**/


getValidate : (email,fn) => {
    dynamodb.getItem({
      TableName: config.PRINTERS_NAME,
      key: {
          mail: {
            S: email
          }
      }
    }, function(err, data){
        if (err) return fn(err)
        else {
          if ('Item' in data){
            var verified = data.Item.verified.BOOL;
            var verifyToken = null;
            if(!verified){
              verifyToken = data.Item.verifyToken.S;
            }
            fn(null, verified,verifyToken);
          }else{
            fn(null, null);
          }
        }
    });
},

/**
**  Function that activates printer's account
**  @email : the email of the printer to activate
** @fn : a callback function
**/

validate : (id,token,fn) => {

  dynamodb.getItem({
    TableName: config.PRINTERS_NAME,
    Key: {
      printer_id : {
        S: id
      }
    },
    ProjectionExpression : "token"
  }, function(err,data){
      if ('Item' in data){
        if(data.Item.token.S == token){
          //Update verified
          var params = {
            TableName : config.PRINTERS_NAME,
            Key : {
              id : {
                S : id
              }
            },
            UpdateExpression : "set verified = true"
          }

          dynamodb.updateItem(params, function(err,data){
            if(!err) fn(JSON.stringify({
              "codeReturn" : "1",
              "printer_id" : id
            }));
          })
        }
      }
  });
},

/**
** Function taking the email and the token given by the printer
** to authenticate himself
** @email : email of the printer
** @verify : token to verify for authentication
**/
valid : (email, verify) => {
  getValidatePrinter(email, function(err, verified, correctToken){
    if(err){
      return "0";//Error in validPrinter
    }else if (verified){
      return "1";//Printer already verified
    }else if (verified == verify){
      validatePrinter(email, function(err, data){
          if(err){
            return "2";//Error in update of printer
          }else{
            return "3";//Printer updated successfully
          }
      });
    }else{
      return "4";//Wrong token given
    }
  });
},

getToken : (email, fn) => {
  var param = {
      IdentityPoolId: config.IDENTITY_POOL_ID,
      Logins : {}
  };

  param.Logins[config.DEVELOPER_PROVIDER_NAME]= email;
  cognitoidentity.getOpenIdTokenForDeveloperIdentity(param,
    function(err, data){
        if(err) return fn(err);
        else fn(null, data.IdentityId, data.Token);
    });
},

/**
**  Function to login a printer in his account
** @email : email of printer
** @password : entered by a printer
**/

login : (email, password,fn) => {
  console.log("Trying to log")
  docuClient.scan({
    TableName: config.PRINTERS_NAME,
    FilterExpression : "mail = :email",
    ExpressionAttributeValues : {
      ":email" : email 
    }
  }, function(err, data){
    if(err){
      console.log(err)
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't query the database"
      }))
    }
      if (data.Items.length != 0){
        var obhash = data.Items[0].secret_key
        var obsalt = data.Items[0].passSalt
        var obverified = data.Items[0].verified.BOOL;
        var printerid = data.Items[0].printer_id;

        if(obhash == null){
          fn(JSON.stringify({"codeReturn":"0", "message":"user not found"})); //User not found
        }else {
          computeHash(password, obsalt, function(err, hash){
            if(err) fn(JSON.stringify({"codeReturn" : "-1", "message": "error computing hash"})); //Error
            else {
              if (obhash == hash.toString('base64')){
                //User logged in
                 
                        fn(JSON.stringify({"codeReturn": "1", "message": "authenticating done","id":printerid,"printer_id":printerid}));
                      
                  
              }else{
                //User not logged in
                fn(JSON.stringify({"codeReturn":"-1", "message":"Login failed"}));
              }
            }
          });
        }
      }

    else {
      fn(JSON.stringify({"codeReturn":"0", "message":"Bad credentials"}));
    }
  });
},

/**
** Retrieves Printer's info with his printer id
** @printerid : The id of the printer
**/

getInfo : (printerid,fn) => {
  dynamodb.getItem({
    TableName : config.PRINTERS_NAME,
    Key : {
      "printer_id" : {
        "S": printerid
      }
    }
  }, function(err, data){
      if(err) {
        console.log(err)
        fn(JSON.stringify({"codeReturn": "-1", "message": "Error during retrieval"}));
      }
      else {
        if('Item' in data){

            fn(JSON.stringify({
              "codeReturn" : "1",
              "printer_id" : data.Item.printer_id.S,
              "name" : data.Item.nom.S,
              "telephone": data.Item.telephone.S,
              "mail" : data.Item.mail.S,
              "balance" : data.Item.balance.N,
              "logo_url" : data.Item.logo_url.S,
              "available" : data.Item.available.N,
              "location" : data.Item.location.M.latitude.S+","+data.Item.location.M.longitude.S,
              "balance_history" : data.Item.balance_history.L
            }));
                    }
        else{
          fn(JSON.stringify({"codeReturn": "0", "message":"No printer with corresponding id"}));
        }
      }
  });
},

/**
** Retrieves Prices details on a printer with his printer id
** @printerid : The id of the printer
**/

getPrices : (printerid,fn) => {
  dynamodb.getItem({
    TableName : config.PRINTERS_NAME,
    Key : {
      "printer_id" : {
        "S": printerid
      }
    }
  }, function(err, data){
      if(err) fn(JSON.stringify({"codeReturn": -1, "message": "Error during retrieval"}));
      else {
        if('Item' in data){
            var prices = data.Item.prices.M;
          fn(JSON.stringify({
                "codeReturn" : "1",
                "colorA4" : prices.color_A4.N,
                "colorA3" : prices.color_A3.N,
                "colorA5" : prices.color_A5.N,
                "colorRecto" : prices.color_rectoverso.N,
                "grayA4" : prices.gray_A4.N,
                "grayA5" : prices.gray_A5.N,
                "grayA3" : prices.gray_A3.N,
                "grayRecto" : prices.gray_rectoverso.N,
                "rayure" : prices.rayure.N,
                "serseau" : prices.serseau.N
            }));
        }else{
         fn(JSON.stringify({"codeReturn":0, "message":"No printer with corresponding id"}));
        }
      }
  });
},

updateLogoUrl : (printer_id,logo_url, fn) => {
  var params = {
    TableName : config.PRINTERS_NAME,
    Key : {
      printerid : {
        S: printerid
      }
    },
    UpdateExpression : "set logo_url = :logo",
    ExpressionAttributes:{
      ":logo_url" : logo_url
    },
    ReturnValues : "UPDATED_NEW"
};

  docuClient.update(params, function(err,data)
  {
    if(err) fn(JSON.stringify({"codeReturn":-1, "message":"Error during update"}));
    else fn(JSON.stringify({"codeReturn": 0, "message" : "Update successfull"}));
  });

},

/**
** Update the prices for a printer id
** @printerid the id of the printer
** @prices the json of the price info
**/

updatePrices : (printerid, prices,fn) => {

  var newPrice = {
                "color_A4" : prices.colorA4,
                "color_A3" : prices.colorA3,
                "color_A5" : prices.colorA5,
                "color_rectoverso" : prices.colorRecto,
                "gray_A4" : prices.grayA4,
                "gray_A5" : prices.grayA5,
                "gray_A3" : prices.grayA3,
                "gray_rectoverso" : prices.grayRecto,
                "rayure" : prices.rayure,
                "serseau" : prices.serseau
  }
  var params = {
    TableName : config.PRINTERS_NAME,
    Key : {
      printer_id : printerid
    },
    UpdateExpression : "set prices = :prices",
    ExpressionAttributeValues:{
      ":prices" : newPrice
    }
};

  docuClient.update(params, function(err,data)
  {
    if(err) {
      console.log(err)
      fn(JSON.stringify({"codeReturn":"-1", "message":"Error during update"}));
    }
    else fn(JSON.stringify({"codeReturn": "0", "message" : "Update successfull"}));
  });

},


/**
** Retrieve the tips for a printer
** @printerid the id of the printer
**/

getTips : (printerid,fn) => {
  var params = {
    TableName : config.TIPS_NAME,
    FilterExpression : "printer_id = :p",
    ExpressionAttributeValues : {
      ":p" : printerid
    }
  }

  docuClient.scan(params, function(err,data){
      if(err){
        console.log("Tips Error\n"+err)
        fn(JSON.stringify({
          "codeReturn" : "-1",
          "message" : "Error during retrieval"
        }));
      }else{
        console.log("Tips ok ....")
        var list = []
        data.Items.forEach(function(element,index,array){
            list.push({
              "tipid" : element.tip_id,
              "printer_id" : printerid,
              "description" : element.description,
              "name" : element.name,
              "tip_url" : element.tip_url
            });

        });

        fn(JSON.stringify({
          "codeReturn" : "1",
          "tips" : list
        }))
      }
  });
},

/**
**  Retrieve a number of tips from the printer
** @printerid : The id of the printer
** @number : the number of tips willing to be retrieved
**/
getTipsNumber : (printerid,number,fn) => {

  var params = {
    TableName : config.TIPS_NAME,
    FilterExpression : "printer_id = :p",
    ExpressionAttributeValues : {
      ":p" : printerid
    }
  }

  docuClient.scan(params, function(err,data){
      if(err){
        fn(JSON.stringify({
          "codeReturn" : "-1",
          "message" : "Error during retrieval"
        }));
      }else{
        var list = []
        var tipsCounter = 0
        data.Items.forEach(function(element,index,array){
            list.push({
              "tip_id" : element.tip_id,
              "tipid" : element.tip_id,
              "printer_id" : printerid,
              "description" : element.description,
              "name" : element.name,
              "tip_url" : element.tip_url
            });
        });

        fn(JSON.stringify({
          "codeReturn" : "1",
          "tips" : list.slice(0,number)
        }))
      }
  });
},

/**
** Retrieve a tip by its id
** @printerid : the printer id
** @tipid : the id of the tip to be retrieved
**/

getTipsById : (tipid,fn) => {

  var params = {
    TableName : config.TIPS_NAME,
    Key : {
      tip_id : {
        S: tipid
      }
    }
  };

  dynamodb.getItem(params, function(err,data){
    if(err) fn(JSON.stringify({"codeReturn": -1, "message": "Error during retrieval"}));
    else {
      if('Item' in data){

          var returnArray = {
            "tip_id" : tipid,
            "tipid" : tipid,
            "printer_id" : data.Item.printer_id.S,
            "description" : data.Item.description.S,
            "name" : data.Item.name.S,
            "tip_url" : data.Item.tip_url.S,
            "codeReturn" : "1",
            "message" : "well retrieved"
          }
          fn(JSON.stringify(returnArray));
        }else{
          fn(JSON.stringify({
            "codeReturn" : "0",
            "message" : "Item not found"
          }));
        }
      }
  });
},

/**
** Create a tip for a printer
** @printerid : the printer id of the printer to create
** @description : the description of the tip
** @name : Name of the tip
**/

createTip : (printerid, description,uploadname, name,fn) => {


  const tip_id = uuid.v1()

  var tipsJson = {
    "tip_id" : tip_id,
    "printer_id" : printerid,
    "description" : description,
    "tip_url" : uploadname,
    "name" : name
  }

  var params = {
    TableName : config.TIPS_NAME,
    Item : tipsJson
  }

  docuClient.put(params, function(err,data){
      if(err) {
        fn(JSON.stringify({
          "codeReturn" : "-1",
          "message" : "Error processing the creation"
        }));
      }else{
        fn(JSON.stringify({
          "codeReturn" : "1",
          "tip_id" : tip_id
        }))
      }
    })

},

/**
** Update a tip
** @tipid : the id of the tip
** newtip : new json file of the tip
**/

updateTip : (tip_id, newtip,fn) => {

  var params = {
    TableName : config.TIPS_NAME,
    Key : {
      "tip_id" : tip_id
    },
    UpdateExpression : "set #name = :newname,  #description = :newdesc",
    ExpressionAttributeValues : {
      ":newname" : newtip.name,
      ":newdesc" : newtip.description
    },
    ExpressionAttributeNames : {
      "#name" : "name",
      "#description" : "description"
    }

  };

  docuClient.update(params, function(err, data){
    if(err) {
      console.log(err)
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Update not achieved"
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Tip well updated"
      }));
    }
  });

},

/**
** Delete tip
** @tipid : the id of the tip
** @printerid : the printer's id
**/

deleteTip : (tipid,fn) => {
  var params = {
    TableName : config.TIPS_NAME,
    Key : {
      "tip_id" : tipid
    }
  }

  docuClient.delete(params, function(err,data){
    if(err) {
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't delete tip"
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Tip deleted"
      }));
    }
  });
},


/**
**  Return reviews stats for a given tip
** @tipid : the tip id we which the stats
**/

getTipStats : (tipid,fn) => {
  CustomerService.getAllTipReviewsById(tipid, function(reviews){
    reviews = JSON.parse(reviews)
    if(reviews.codeReturn == "1"){
      console.log("Tips reviews loading ... \n")
        //Compute the reviews
        var goodReviews = 0
        var neutralReviews = 0
        var badReviews = 0

        for (review in reviews.reviews)
        {
          if(review.review == 1) goodReviews++;
          if(review.review == 0) neutralReviews++;
          if(review.badReviews == -1) badReviews++;
        }

        fn(JSON.stringify({
          "codeReturn" : "1",
          "reviews" : {
            "goodReviews" : goodReviews,
            "neutralReviews" : neutralReviews,
            "badReviews" : badReviews,
          }
        }));

    }else{
      console.log("Error")
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Error during reviews retrieval"
      }))
    }
  });
},

/**
** Notify tip to customers
** @printerid : the printer id of the printer willing to notifify
** @tipid : the tip id of the tip to notify customers
** @name : the name of the printer
** [TODO] : Save the fact the tip has been saved
** [think of retrieving tips stats] from user Feedbacks
**/

notifyTip : (printerid, name, tipid,fn) => {
  //Get CustomerMessenger Id list
  CustomerService.getAllIds(function(Idslist){
    Idslist = JSON.parse(Idslist)
    if(Idslist.codeReturn == "-1"){
      console.log("Error during list retrieval")
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Error retrieving messenger list"
      }));
    } else{

      Idslist.list.forEach(function(messenger_id){
        var params = {
          url : "https://api.chatfuel.com/bots/"
                + config.EVEYPRINT_BOT_ID +"/users/"
                + messenger_id + "/send",
          method : 'POST',
          qs : {
            "chatfuel_token" : config.CHATFUEL_API_KEY,
            "chatfuel_block_name" : config.EVERYPRINT_TIP_BLOCK,
            "printer_name" : name,
            "tipid" : tipid,
            "printerid" : printerid
          }
        }

        request(params, function(error, response, body){
          if(error){
            console.log("Error during tip notification \n "+error)
        }

        //Set timeout of 0.5s

      });
      })
          fn(JSON.stringify({
              "codeReturn" : "1",
              "message" : "Successfully achieved"
          }));



    }
  })

  //Send via chatfuel api
  //timeout 1s
},

/**
** This function sends the tips data so that chatfuel can send it to
** the user calling this function
**/
sendTipData : (printername, tipid,fn) => {
  module.exports.getTipsById(tipid, function(tip){
    tip = JSON.parse(tip)
    if(tip.codeReturn == "1"){

      /*
        {
            "text" : "Trouvez ce message interessant ? ",
            "quick_replies" :[
                   {
                      "title" : "Oui",
                      "url" : apiBaseUrl+"recordTipReview?tip_id="+tipid+"&review=1&customer_id={{messenger user id}}"
                   },
                   {
                      "title" : "Aucune idée",
                      "url" : apiBaseUrl+"recordTipReview?tip_id="+tipid+"&review=0&customer_id={{messenger user id}}"
                   },
                   {
                      "title" : "Non",
                      "url" : apiBaseUrl+"recordTipReview?tip_id="+tipid+"&review=-1&customer_id={{messenger user id}}"
                   }
            ]
          }
      */

      fn(JSON.stringify({
        "messages": [
          {
              "text" : "Hey, voici un nouveau message provenant de l'imprimeur "+printername
          },
          {
              "text" : tip.description
          },
          {
            "attachment" : {
              "type" : getFileType(tip.tip_url),
              "payload" : {
                "url" : tip.tip_url
              }
            }
          },
          {
              "text" : "N'oubliez pas, pour imprimer un document, tapez Imprimer, à plus :)."
          }

        ],
        "codeReturn" : "1"
      }))
    }else{
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Error during tip information retrieve"
      }))
    }
  });

},

/**
** Create an available file for the printer and automatically publish it
** @printerid : the printer's id
** @name : the name of the available
** @uploadname : the name for the file upload
** @validity : the period of validity for the file
**/

createAvailFile : (printerid,name,uploadname,validity,fn) => {


      const avail_id = uuid.v1()

      var availJson = {
        "avail_id" : avail_id,
        "printer_id" : printerid,
        "validity" : validity,
        "url" : uploadname,
        "name" : name,
        "reserved" : 0
      }

      var params = {
        TableName : config.AVAIL_NAME,
        Item : availJson
      }

      docuClient.put(params, function(err,data){
        if(err){
           fn(JSON.stringify({
          "codeReturn" : "-1",
          "message" : "Couldn't create the available file"
        }));
      }else{
        fn(JSON.stringify({
          "codeReturn" : "1",
          "id" : avail_id
        }));
      }


      });


},

/**
** Get available files by id
** @printerid : the printer's id
** @availid : the avail id we which the infos
**/
getAvailsById : (printerid, availid,fn) => {
  var params = {
    TableName :AVAIL_NAME,
    Key : {
      avail_id : {
        S: availpid
      },
      printer_id : {
        S: printerid
      }
    }
  };

  dynamodb.getItem(params, function(err,data){
    if(err) fn(JSON.stringify({"codeReturn": -1, "message": "Error during retrieval"}));
    else {
      if('Item' in data){

          var returnArray = {
            "avail_id" : tipid,
            "printer_id" : printerid,
            "name" : data.Item.name.S,
            "validity" : data.Item.validity.S,
            "reserved" : data.Item.reserved.N,
            "url" : data.Item.url.S,
            "codeReturn" : "1"
          }
          fn(returnArray);
        }else{
          fn(JSON.stringify({
            "codeReturn" : "0",
            "message" : "Item not found"
          }));
        }
      }
  });


},

/**
** Get available files by id
** @printerid : the printer's id
** @number : the number of available files
**/
getAvailsNumberById : (printerid,number,fn) => {
  var params = {
    TableName : config.AVAIL_NAME,
    FilterExpression : "printer_id = :p",
    ExpressionAttributeValues : {
      ":p" : printerid
    }

  }

  docuClient.scan(params, function(err,data){
      if(err){
        fn(JSON.stringify({
          "codeReturn" : "-1",
          "message" : "Error during retrieval"
        }));
      }else{
        var list = []
        var availsCounter = 0
        data.Items.forEach(function(element,index,array){
            list.push({
              "avail_id" : element.avail_id,
              "printer_id" : printerid,
              "validity" : element.validity,
              "name" : element.name,
              "file_url" : element.url,
              "reserve" : element.reserved
            });

        });

        fn(JSON.stringify({
          "codeReturn" : "1",
          "files" : list.slice(0,number)
        }))
      }
  });
},

/**
** Get all available files for a given printer
** @printerid : the printer's id
**
**/

getAvailsAllById: (printerid, fn) => {
  var params = {
    TableName : config.AVAIL_NAME,
    FilterExpression : "printer_id = :p",
    ExpressionAttributeValues : {
      ":p" : printerid
    }

  }

  docuClient.scan(params, function(err,data){
      if(err){
        fn(JSON.stringify({
          "codeReturn" : "-1",
          "message" : "Error during retrieval"
        }));
      }else{
        var list = []
        data.Items.forEach(function(element,index,array){
            list.push({
              "avail_id" : element.avail_id,
              "printer_id" : printerid,
              "validity" : element.validity,
              "name" : element.name,
              "file_url" : element.url,
              "reserve" : element.reserved
            });
        });

        fn(JSON.stringify({
          "codeReturn" : "1",
          "files" : list
        }))
      }
  });
},

/**
** Update or reset parameter for reserve number of an available file
** @printerid : the id of the printer
** @availid : the id of the available file
** @number : increase the number of reservation by number
** @reset : if set, reset the number of reservation to zero
[TODO]: Redesign the database after placing the commands
**/
updateAvailReserve : (printerid, availid, number, reset,fn) => {
  if(reset == "0")
  {
    var params = {
      TableName : AVAIL_NAME,
      Key : {
        "printer_id" : printerid,
        "avail_id" : availid
      },

      UpdateExpression : "set :reserved = :reserved + :val",
      ExpressionAttributes : {
        ":reserved" : reserved
      },
      ExpressionAttributeValues : {
        ":val" : number
      }
    }
}else{
  var params = {
    TableName : AVAIL_NAME,
    Key : {
      "printer_id" : printerid,
      "avail_id" : availid
    },
    UpdateExpression : "set reserved = 0"
  }
}

docuClient.update(params, function(err,data){
  if(err) {
    fn(JSON.stringify({
      "codeReturn" : "-1",
      "message" : "Error during the update"
    }));
  }else{
    fn(JSON.stringify({
      "codeReturn" : "1",
    }));
  }
});

},
/**
** Notify available file
** @printerid : the printer's id
** @name : the printer's name
** @avail_id : the available file id
**/

notifyAvail : (fn) => {
  //Get CustomerMessenger Id list
  Idslist = CustomerService.getAllIds(function(Idslist){
    Idslist = JSON.parse(Idslist)
    if(Idslist.codeReturn == "-1"){
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Error retrieving messenger list"
      }));
    } else{
      for (messenger_id in Idslist.list)
      {
        var params = {
          url : "https://api.chatfuel.com/bots/"
                + config.EVEYPRINT_BOT_ID +"/users/"
                + messenger_id + "/send",
          method : 'POST',
          qs : {
            "chatfuel_token" : config.CHATFUEL_API_KEY,
            "chatfuel_block_name" : config.EVERYPRINT_AVAILABLE_FILE,
            "printer_name" : name,
            "avail_id" : tipid,
            "printerid" : printerid
          }
        }

        request(params, function(error, response, body){
          if(error){
            fn(JSON.stringify({
              "codeReturn" : "0",
              "message" : "Error during post",
              "body" : body
            }));
        }

        //Set timeout of 0.5s

      });
    }
          fn(JSON.stringify({
              "codeReturn" : "1",
              "message" : "Successfully achieved"
          }));



    }
    //Send via chatfuel api
    //timeout 1s

  })
  },

/**
** Update a reserve file
** @printerid : the printer's id
** @availid : the available file id
** newinfo : json containing the new name and validity
**/

updateReservedFile : (printerid, availid, newinfo,fn) => {
  var params = {
    TableName : config.AVAIL_NAME,
    Key : {
      "printer_id" : printerid,
      "avail_id" : availid
    },
    UpdateExpression : "set name = :newname, validity = :newvalidity",
    ExpressionAttributeValues : {
      ":newname" : newinfo.name,
      ":newvalidity" : newinfo.newvalidity
    }
  }

  docuClient.update(params, function(err,data){
    if(err){
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Error during the update"
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "updated" : "1"
      }));
    }
  });

},

/**
** Delete a reserve file
** @printerid : the printer's id
** @availid : the available file id to delete
**/

deleteReservedFile : (printerid, availid,fn) => {
  var params = {
    TableName : AVAIL_NAME,
    Key : {
      "printer_id" : printerid,
      "avail_id" : availid
    }
  }

  docuClient.delete(params, function(err, data){
    if(err) {
      fn(JSON.stringify({
        "codeReturn" : "-1"
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1"
      }))
    }
  });
},


/**
** Send available file notification
**/

/**
** Update Printers balance
** @printerid : the printer's id of the printer to update the balance
** @balance : the balance to update
** @flag : 0 - add, 1 - substract
**/

updateBalance : (printerid, balance, flag,fn) => {
  if(flag == 0)
  {
    var expression = "set #balance = #balance + :balance";
  }else {
    var expression = "set #balance = #balance - :balance";
  }
  var params = {
    TableName : config.PRINTERS_NAME,
    Key : {
      printer_id : {
        S : printerid
      }
    },
    UpdateExpression : expression,
    ExpressionAttributes : {
      "#balance" : "balance"
    },
    ExpressionAttributeValues : {
      ":balance" : balance
    }
  }

  dynamodb.updateItem(params, function(err, data){
    if (err){
      fn(JSON.stringify({
        "codeReturn" : "0",
        "message" : "Couldn't update printer balance"
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Successful balance update",
      }));
    }
  });
},

/**Reset the balance of the printer
** @printer_id : the id of the printer
**/

resetBalance : (printerid,fn) => {

  var params = {
    TableName : config.PRINTERS_NAME,
    Key : {
      printer_id : {
        S : printerid
      }
    },
    UpdateExpression : "set balance = 0"
  }

  dynamodb.updateItem(params, function(err, data){
    if (err){
      fn(JSON.stringify({
        "codeReturn" : "0",
        "message" : "Couldn't update printer balance"
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Successful balance update",
      }));
    }
  });
},


/**
** Get 10 printer ids
** @number : the number of ids willing to be retrived
**/

getPrintersIds  : (number,fn) => {

  nb = (number == null) ? 10 : number 
  var params = {
    TableName : config.PRINTERS_NAME,
    Limit : nb,
    ProjectionExpression : "nom,printer_id,prices,logo_url",
    FilterExpression: 'available = :s',
    ExpressionAttributeValues: {
      ':s': 1
    }



  }
  var list = []
  docuClient.scan(params, function(err,data){
      if(err){
        console.log("error + "+err)
        fn(JSON.stringify({
          "codeReturn" : "-1",
          "message" : "Error during ids retrieval"
        }));
      }else{
        var counter = 0;
        
        console.log("This is the printer's list\n" + list)
    
        fn(JSON.stringify({
          "codeReturn" : "1",
          "list" : data.Items
        }))
      }
     
  });

},

/**
** Verify if an entry in perdaystat table for a given printer and day is already present
** @printer_id : the id of the printer
**  @day : the day
**/

doesStatDayValid : (printer_id, day,fn) => {
  var params = {
    TableName : config.PerDayStats,
    Key : {
      printer_id : {
        S : printer_id
      },
      date : {
        S : date
      }
    },
    ProjectionExpression : "printer_id,date"
  }

  dynamodb.getItem(params, function(err,data){
    if(err) fn(JSON.stringify({
      "codeReturn" : "-1",
      "message" : "Error during querying"
    }));
    else {
      if('Item' in data) fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Entry already present"
      }));

      else{
        //Create entry
        dynamodb.putItem({
          TableName : config.PerDayStats,
          Item : {
            id : {
              S: uuid.v1()
            },
            printer_id : {
              S : printer_id
            },
            balance : {
              N : 0
            },
            nbcommands : {
              N : 0
            }
          }
        }, function(err,data){
          if(err) fn(JSON.stringify({
            "codeReturn" : "-1",
            "message" : "Couldn't create new entry"
          }));
          else{
            fn(JSON.stringify({
              "codeReturn" : "1",
              "message" : "Create successful"
            }));
          }
        });
      }
    }

  });
},

/**
** Update perDayStatistics
** @printer_id : the id of the printer
** @day : the day of the stats
** @commands_done : the number of commands done
** @balance : the balance done since the first
**/

updatePerDay : (printer_id,date,balance,fn) => {


  doesStatDayValid(printer_id,day, function(valid){
    if(valid.codeReturn == "-1"){
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't update"
      }))
    }

    var params = {
      TableName : config.PerDayStats,
      Key : {
        printer_id : {
          S : printer_id
        },
        date : {
          S : date
        }
      },
      UpdateExpression : "set #commands_done = #commands_done+1, #balance = #balance + :balance",
      ExpressionAttributes : {
        "#commands_done" : "nbcommands",
        "#balance" : "balance"
      },
      ExpressionAttributeValues : {
        ":balance" : balance
      }
    }

    docuClient.updateItem(params,function(err,data){
      if(err) {
        fn(JSON.stringify({
          "codeReturn" : "-1",
          "message" : "Error during update"
        }));
      }else{
        fn(JSON.stringify({
          "codeReturn" : "1",
          "message" : "Update successful"
        }));
      }
    });
  });
  //Does the day exists ???


},

/**
** Get per day perDayStatistics
** @printer_id : the id of the printer
**/

getPerDayStat : (printer_id,fn) => {
  var params = {
    TableName : config.PerDayStats,
    KeyConditionExpression : "printer_id = :p",
    ExpressionAttributeValues : {
      ":p" : printer_id
    }

  }
  var list = [];

  docuClient.query(params, function(err,data){
      if(err) fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't retrieve per day stats"
      }));

      data.Items.forEach(function(element,index,array){
        list.push({
          "day" : element.date,
          "commands_done" : element.nbcommands,
          "balance" : element.balance
        });
      });

      fn(JSON.stringify({
        "codeReturn" : "1",
        "stats" : list
      }));
  });
},
/**
** Get per day perDayStatistics
** @printer_id : the id of the printer
** @number : the number of statistictics to retrieve
**/

getPerDayStatNumber : (printer_id,number,fn) => {
  var params = {
    TableName : config.PerDayStats,
    KeyConditionExpression : "printer_id = :p",
    ExpressionAttributeValues : {
      ":p" : printer_id
    }

  }
  var list = [];

  docuClient.query(params, function(err,data){
      if(err) fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't retrieve per day stats"
      }));
      var counter = 0
      data.Items.forEach(function(element,index,array){
        list.push({
          "day" : element.date.S,
          "commands_done" : element.nbcommands.N,
          "balance" : element.balance.N
        });

      });

      fn(JSON.stringify({
        "codeReturn" : "1",
        "stats" : list.slice(0,number)
      }));
  });
},
/**
** Get per day perDayStatistics
** @printer_id : the id of the printer
** @date : for a particular date
**/

getPerDayStatDate : (printer_id,date,fn) => {
  var params = {
    TableName : config.PerDayStats,
    Key : {
      printer_id : {
        S : printer_id
      },
      date : {
        S : date
      }
    }
  }
  var list = [];

  dynamodb.getItem(params, function(err,data){
      if(err) fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't retrieve per day stats"
      }));

      if('Item' in data){
        fn(JSON.stringify({
          "codeReturn" : "1",
          "list" : [
            {
              "day" : data.Item.date.S,
              "commands_done" : data.Item.nbcommands.N,
              "balance" : data.Item.balance.N
          }
        ]
      }))
      }else{
        fn(JSON.stringify({
          "codeReturn" : "0",
          "list" : []
        }));
      }
  });
},


/**
** Withdraw the balance of the printer to
** @printer_id : the printer id
** @pass_key : the pass key issued
**/

withdrawAccount : (printer_id,pass_key,fn) => {
  //Verify pass_key
  dynamodb.getItem({
    TableName: config.PRINTERS_NAME,
    key : {
      printer_id : {
        S: printer_id
      }
    }
  }, function(err, data){
    if(err) return "error";
    else{
      if ('Item' in data){
        var obhash = data.Item.secret_key.S;
        var obsalt = data.Item.passSalt.S;
        var obverified = data.Item.verified.BOOL;
        var printerid = data.Item.printer_id.S;

        if(obhash == null){
          fn(JSON.stringify({"codeReturn":"-1", "message":"user not found"})); //User not found
        }else if (!obverified){
          fn(JSON.stringify({"codeReturn":"-1", "message":"user credentials not good"})); //User not well authentified
        }else {
          computeHash(password, salt, function(err, salt, hash){
            if(err) fn(JSON.stringify({"codeReturn" : "-1", "message": "error computing hash"})); //Error
            else {
              if (obhash == hash){
                          //Credentials are good

                          this.getInfo(printer_id,function(printerInfo){
                            printerInfo = JSON.parse(printerInfo)
                            if(printerInfo.codeReturn == "-1"){
                              fn(JSON.stringify({"codeReturn":"-1", "message":"Couldn't obtain printer balance info"})); //Error getting token

                            }else{
                              var options = {
                                url : config.withdrawal_url,
                                qs : {
                                  "service_key" : config.MONET_BIL_SERVICE_KEY,
                                  "service_secret" : config.MONET_BIL_SERVICE_SECRET,
                                  "phonenumber" : printerInfo.telephone,
                                  "amount" : printerInfo.balance
                              },
                              method : "POST"
                            }

                            request(options,function(err,response, body){
                              if(!err && response.statusCode == 200){
                                if(body.success) {

                                  //Update the BalanceHistory
                                  this.updateBalanceHistory(printer_id,new Date(),function(returnValue){
                                    returnValue = JSON.parse(returnValue)
                                    if(returnValue.codeReturn == "1"){
                                      this.resetBalance(printer_id, function(returnVal){
                                        returnVal = JSON.parse(returnVal)
                                        if(returnVal.codeReturn == "1"){
                                          fn(JSON.stringify({
                                              "codeReturn" : "1",
                                              "message" : "Update successful"
                                            }));
                                        }
                                      });
                                    }
                                  });


                                }
                                else fn(JSON.stringify({
                                  "codeReturn" : "0",
                                  "message" : "Couldn't retrieve the cash to the account"
                                }));
                              }else{
                                fn(JSON.stringify({
                                  "codeReturn" : "-1",
                                  "message" : "Error during withdrawal"
                                }));
                              }
                            });
                        }
                          });



              }else{
                //User not logged in
                fn(JSON.stringify({"codeReturn":3, "message":"Login failed"}));
              }
            }
          });
        }
      }
    }});
  //Send money
},

/**
** Get BalanceHistory retrieval
**/

getBalanceHistory : (printer_id,fn) => {
  var params = {
    TableName : config.PRINTERS_NAME,
    Key : {
      printer_id : {
        S : printer_id
      }
    },
    ProjectionExpression : "balance_history"
  }

  var historyList = [];

  dynamodb.getItem(params, function(err,data){
      if(err) fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't obtain balance history"
      }));

      if('Item' in data){
        fn(JSON.stringify({
          "codeReturn" : "1",
          "history" : data.Item.balance_history.L
        }));
      }else{
        fn(JSON.stringify({
          "codeReturn" : "0",
          "message" : "No printer found with the corresponding id"
        }))
      }
    });

},

/**
** Get BalanceHistory retrieval by number
**/

getBalanceHistoryByNumber : (printer_id, number, fn) => {
  var params = {
    TableName : config.PRINTERS_NAME,
    Key : {
      printer_id : {
        S : printer_id
      }
    },
    ProjectionExpression : "balance_history"
  }

  var historyList = [];

  dynamodb.getItem(params, function(err,data){
      if(err) fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't obtain balance history"
      }));

      if('Item' in data){
        historyList = data.Item.balance_history.L;

        fn(JSON.stringify({
          "codeReturn" : "1",
          "history" : historyList.slice(0,number)
        }));
      }else{
        fn(JSON.stringify({
          "codeReturn" : "0",
          "message" : "No printer found with the corresponding id"
        }))
      }
    });

},

/**
** UpdateBalanceHistory
** @printer_id : the printer id
** @amount : the amount to append to the list
** @date : the date for the retrieval
**/

updateBalanceHistory : (printer_id, amount, date,fn) => {
  var newHistory = {
    "date" : date,
    "retrieved" : amount
  }
  var params = {
    TableName : config.PRINTERS_NAME,
    Key : {
      printer_id : {
        S : printer_id
      }
    },
    UpdateExpression : "set #balancehistory = list_append(#balancehistory,:newhistory)",
    ExpressionAttributes : {
      "#balancehistory" : "balance_history"
    },
    ExpressionAttributesValues : {
      ":newhistory" : newHistory
    }
  }

  docuClient.update(params, function(err,data){
    if(err){
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't update "
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Update successful"
      }));
    }
  })
},

/**
** Change state for availability of the printer
** @printer_id : the id of the printer
** @state : the state of the printer
**/
changeAvailableState : (printer_id, state,fn) => {
  var params = {
    TableName : config.PRINTERS_NAME,
    Key : {
      printer_id : {
        S : printer_id
      }
    },
    UpdateExpression : "set available = :state",
    ExpressionAttributeValues : {
      ":state" : state
    }
  }

  docuClient.update(params, function(err,data){
    if(err){
      console.log(err)
      fn(JSON.stringify({
        "codeReturn" : "-1",
        "message" : "Couldn't change availability state",
        "state" : null
      }));
    }else{
      fn(JSON.stringify({
        "codeReturn" : "1",
        "message" : "Could change availability state",
        "state" : state,
        "printer_id" : printer_id
      }));
    }
  });
}



}

const GraphQL = require('graphql');
const {
	GraphQLObjectType,
	GraphQLString,
	GraphQLID,
	GraphQLInt,
  GraphQLFloat,
	GrapqhQLNonNull,
	GraphQLList,
  GraphQLInputObjectType
} = GraphQL;



const CommandMutation = {
  saveCommand : {

  },
  computeCommand : {

  },
  finishCommand : {

  },
  retrieveCommand : {

  },
  updateCommandState : {
    type : new GraphQL.GraphQLNonNull(UpdateStateType),
    description : "Update the state of a command (0 paid, 1 finished, 2 retrieved)",
    args : {
      command_id : {
        type : new GraphQL.GraphQLNonNull(GraphQLString),
        description : "The id of the command we wish to change the state"
      },
      state : {
        type : new GraphQL.GraphQLNonNull(GraphQLString),
        description : "The new state of the command (0 paid, 1 finished, 2 retrieved)"
      }
    },
    resolve : (rootValue, { command_id }, { state }) => {
      return new Promise((resolve,reject)=>{
        CommandService.changeState(command_id,state,function(result){
          resolve(JSON.parse(result))
        })
      })
    }


  }
}

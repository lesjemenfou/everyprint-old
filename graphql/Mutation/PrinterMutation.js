const GraphQL = require('graphql');
const {
	GraphQLObjectType,
	GraphQLString,
	GraphQLID,
	GraphQLInt,
  GraphQLFloat,
	GrapqhQLNonNull,
	GraphQLList,
	GraphQLInputObjectType,
	GraphQLOutputObjectType
} = GraphQL;


const Prices  = require('../types/Prices')
const PrinterService = require('../dbservice/PrinterService')
const CommandService = require('../dbservice/CommandService')

const ReturnCreationAllType = new GraphQL.GraphQLObjectType({
  name: "ReturnCreationAllType",
  description: "The return value for majority",
  fields: () => ({
    codeReturn : {
      type : GraphQLString,
      description : "Code return of the function. 1 means good otherwise error/not achieved"
    },
    message : {
      type : GraphQLString,
      description : "Debug message from the server."
    },
    id : {
    	type : GraphQLString,
    	description : "The id of the object created"
    }
  })
})

const UpdateStateType = new GraphQL.GraphQLObjectType({
  name: "UpdateStateType",
  description: "The return value for the update of a command",
  fields: () => ({
    codeReturn : {
      type : GraphQLString,
      description : "Code return of the function. 1 means good otherwise error/not achieved"
    },
    message : {
      type : GraphQLString,
      description : "Debug message from the server."
    }
  })
})

const InputCommandState = new GraphQL.GraphQLInputObjectType({
  name: "InputCommandState",
  description: "The necessary fields to create a printer",
  fields: () => ({
    command_id : {
			type : new GraphQL.GraphQLNonNull(GraphQLString),
			description : "The id of the command we wish to change the state"
		},
		state : {
			type : new GraphQL.GraphQLNonNull(GraphQLString),
			description : "The new state of the command (0 paid, 1 finished, 2 retrieved)"
		}
  })
})

const PrinterReturn = new GraphQL.GraphQLObjectType({
  name : "PrinterReturn",
  description : "The necessary fields to return for a printer operation",
  fields : () => ({
    printer_id : {
      type : GraphQLString,
      description : "The id of the printer"
    },
    email : {
      type : GraphQLString,
      description : "The email of the printer"
    }
  })
})

const orderReturn = new GraphQL.GraphQLObjectType({
  name : "orderrReturn",
  description : "The necessary fields to return for a order operation",
  fields : () => ({
    id_order : {
      type : GraphQLString,
      description : "The id of the order"
    },
    codeReturn : {
      type : GraphQLString,
      description : "The code return from the printer"
    }
  })
})

const StateReturn = new GraphQL.GraphQLObjectType({
  name : "StateReturn",
  description : "The necessary fields to return for a printer tate soperation",
  fields : () => ({
    printer_id : {
      type : GraphQLString,
      description : "The id of the printer"
    },
    state: {
      type : GraphQLInt,
      description : "The state of the printer"
    }
  })
})


const priceReturn = new GraphQL.GraphQLObjectType({
  name : "PriceReturn",
  description : "The necessary fields to return for a printer tate soperation",
  fields : () => ({
    printer_id : {
      type : GraphQLString,
      description : "The id of the printer"
    },
    prices: {
      type : Prices,
      description : "The state of the printer"
    }
  })
})


const PricesInput = new GraphQL.GraphQLInputObjectType({
    name : "PricesInput",
    description : "This represents the differents prices offered by this printer",
    fields : () => ({
        colorA4 : {
            type : GraphQLFloat,
            description : "The price for A4 color / page"
        },
        colorA3 : {
            type : GraphQLFloat,
            description : "The price for A3 color / page"
        },
        colorA5 : {
             type : GraphQLFloat,
             description : "The price for A5 color /page"
        },
        colorRecto : {
              type : GraphQLFloat,
              description : "The price to add for rectoverso in color mode"
        },
        grayA4 : {
            type : GraphQLFloat,
            description : "The price for A4 gray / page"
        },
        grayA3 : {
            type : GraphQLFloat,
            description : "The price for A3 gray / page"
        },
        grayA5 : {
             type : GraphQLFloat,
             description : "The price for A5 gray /page"
        },
        grayRecto : {
              type : GraphQLFloat,
              description : "The price to add for rectoverso in gray mode"
        },
        rayure : {
              type : GraphQLFloat,
              description : "The price for a rayure"
        },
        serseau : {
              type : GraphQLFloat,
              description : "The price for a serseau"
        }

    })
});

const PriceInput = new GraphQL.GraphQLInputObjectType({
	name : "UpdatePriceinput",
	description : "The fields necessary to update the prices of a printer",
	fields : () => ({
		printer_id : {
			type : GraphQLString,
			description : "The id of the printer to update the price"
		},
		prices : {
			type : PricesInput,
			description : "The new prices we wish to update"
		}
	})
})

const InputPrinterType = new GraphQL.GraphQLInputObjectType({
  name: "PrinterInputType",
  description: "The necessary fields to create a printer",
  fields: () => ({
    mail : {
      type : GraphQLString,
      description : "The printer's email"
    },
    telephone : {
      type : GraphQLString,
      description : "The printer's telephone number"
    },
    name : {
      type : GraphQLString,
      description : "The printer's name"
    },
    location : {
      type : GraphQLString,
      description : "The printer's location"
    },
    secret_key : {
      type : GraphQLString,
      description : "The secret key of the printer"
    }
  })
})

const InputOrderType = new GraphQL.GraphQLInputObjectType({
  name: "OrderInputType",
  description: "The necessary fields to create an order",
  fields: () => ({
    customer_id : {
      type : GraphQLString,
      description : "The id of the customer concerned"
    },
    income : {
      type : GraphQLString,
      description : "The income of the actual order"
    },
    command_id : {
      type : GraphQLString,
      description : "The first command of the order"
    }
  })
})


const LoginInput = new GraphQL.GraphQLInputObjectType({
	name : "LoginInput",
	description : "The necessary fields to login a user",
	fields : () => ({
		email : {
			type : new GraphQL.GraphQLNonNull(GraphQLString),
			description : "The email of the user"
		},
		password : {
			type : new GraphQL.GraphQLNonNull(GraphQLString),
			description : "The password of the user"
		}
	})
})


const PrinterMutation = new GraphQL.GraphQLObjectType({
  name : "PrintersMutations",
  description : "The set of everyprint mutations",
  fields : ({

    createPrinter : {
      type : new GraphQL.GraphQLNonNull(PrinterReturn),
			description : "Creates a printer from an email, telephone, location and secret key",
      args : {
        input : {
          type : new GraphQL.GraphQLNonNull(InputPrinterType)
        }
      },
      resolve : (rootValue, { input }) => {
        return new Promise((resolve,reject) => {
          PrinterService.save(input,function(data){
						console.log("ddddd")
            resolve(JSON.parse(data))
          });
        })
      }
	},
	createOrder : {
      type : new GraphQL.GraphQLNonNull(orderReturn),
			description : "Creates a printer from an email, telephone, location and secret key",
      args : {
        input : {
          type : new GraphQL.GraphQLNonNull(InputOrderType)
        }
      },
      resolve : (rootValue, { input }) => {
        return new Promise((resolve,reject) => {
          CommandService.createOrder(input.customer_id,input.command_id,input.income,function(data){
						console.log("ddddd")
            resolve(JSON.parse(data))
          });
        })
      }
	},
	addCommandToOrder : {
		type : new GraphQL.GraphQLNonNull(UpdateStateType),
		description : "Add a command to an order",
		args : {
			command_id : {
				type : new GraphQL.GraphQLNonNull(GraphQLString),
				description : "The command id to add to the order"
			},
			order_id : {
				type : new GraphQL.GraphQLNonNull(GraphQLString),
				description : "The order's id where we wish to add a command"
			},
			income : {
				type : new GraphQL.GraphQLNonNull(GraphQLString),
				description : "The income of the command to be added"
			}
		},
		resolve : (rootValue, args) => {
			return new Promise((resolve,reject) => {
				
				CommandService.addCommandToOrder(args.order_id, args.command_id, args.income, function(result){
					resolve(JSON.parse(result))
				})
			})
		}
	},
	changeOrderPrinter : {
		type : new GraphQL.GraphQLNonNull(UpdateStateType),
		description : "Update the printer for the order",
		args : {
			order_id : {
				type : new GraphQL.GraphQLNonNull(GraphQLString),
				description : "The id of the order concerned"
			},
			printer_id : {
				type : new GraphQL.GraphQLNonNull(GraphQLString),
				description : "The id of the printer who will be in charge of the command"
			}
		},
		resolve : (rootValue,args) => {
			return new Promise((resolve,reject)=>{
				
				CommandService.changeOrderPrinterId(args.order_id,args.printer_id,function(result){
					resolve(JSON.parse(result))
				})
			})
		}
	},
	updateTip : {
		type : new GraphQL.GraphQLNonNull(UpdateStateType),
		description : "Update the name, description of a tip",
		args : {
			tip_id : {
				type : new GraphQL.GraphQLNonNull(GraphQLString),
				description : "The id of the tip concerned"
			},
			name : {
				type : new GraphQL.GraphQLNonNull(GraphQLString),
				description : "The new name for the tip"
			},
			description : {
				type : new GraphQL.GraphQLNonNull(GraphQLString),
				description : "The new description for the tip"
			}
		},
		resolve : (rootValue,args) => {
			return new Promise((resolve,reject)=>{
				var newTip = {
					"name" : args.name,
					"description" : args.description
				}
				PrinterService.updateTip(args.tip_id,newTip,function(data){
					resolve(JSON.parse(data))
				})
			})
		}
	},
	deleteTip : {
		type : new GraphQL.GraphQLNonNull(UpdateStateType),
		description : "Delete a tip",
		args : {
			tip_id : {
				type : new GraphQL.GraphQLNonNull(GraphQLString),
				description : "The id of the tip concerned"
			}
		},
		resolve : (rootValue,args) => {
			return new Promise((resolve,reject)=>{
				PrinterService.deleteTip(args.tip_id,function(data){
					resolve(JSON.parse(data))
				})
			})
		}

	},
		updateCommandState : {
			type : new GraphQL.GraphQLNonNull(UpdateStateType),
			description : "Update the state of a command (0 paid, 1 finished, 2 retrieved)",
			args : {
				input : {
					type : new GraphQL.GraphQLNonNull(InputCommandState)
				}
			},
			resolve : (rootValue, { input }) => {
				return new Promise((resolve,reject)=>{
					CommandService.changeState(input.command_id,input.state,function(result){
						resolve(JSON.parse(result))
					})
				})
			}
		},
		updateOrderState : {
			type : new GraphQL.GraphQLNonNull(UpdateStateType),
			description : "Update the state of an order (0 paid, 1 finished, 2 retrieved)",
			args : {
				input : {
					type : new GraphQL.GraphQLNonNull(InputCommandState)
				}
			},
			resolve : (rootValue, { input }) => {
				return new Promise((resolve,reject)=>{
					CommandService.changeOrderState(input.command_id,input.state,function(result){
						resolve(JSON.parse(result))
					})
				})
			}
		},
		loginPrinter: {
			type : new GraphQL.GraphQLNonNull(ReturnCreationAllType),
			description : "Verify if the email and password entered are correct",
			args : {
				input : {
					type : new GraphQL.GraphQLNonNull(LoginInput)
				}
			},
			resolve : (rootValue,{ input })=>{
				return new Promise((resolve,reject)=>{
					PrinterService.login(input.email,input.password,function(result){
						resolve(JSON.parse(result))
					})
				})
			}
		},
			changeAvailableState : {
		
      type : StateReturn,
			description : "Changes the state of a printer to available <-> non available ",
      args : {
        printer_id : {
            type : new GraphQL.GraphQLNonNull(GraphQLString)
        },
        state : {
            type :new GraphQL.GraphQLNonNull(GraphQLString)
        }
      },
      resolve : (rootValue,{ printer_id }, { state }) => {
        return new Promise((resolve,reject) => {
          console.log(state+printer_id)
          PrinterService.changeAvailableState(printer_id,state,function(data){
              resolve(JSON.parse(data))
          })
        })
      }
    },
		updatePrices : {
			type : priceReturn,
			description : "Updates the prices of the printer ",
			args : {
				input : {
					type : new GraphQL.GraphQLNonNull(PriceInput)
				}
			},
			resolve : (rootValue, { input }) => {
				return new Promise ((resolve,reject) => {
					PrinterService.updatePrices(input.printer_id,input.prices,function(data){
						if(JSON.parse(data).codeReturn == "0"){
							resolve({
								"printer_id" : input.printer_id,
								"prices" : input.prices
							})
						}else{
							resolve({
								"printer_id" : input.printer_id,
								"prices" : null
							})
						}
					})
				})
			}
		},
		notifyTip : {
			type : GraphQLString,
			description : "Notify the tip to the user in order to get info about it",
			args : {
				tip_id : {
					type : new GraphQL.GraphQLNonNull(GraphQLString)
				},
				printer_id : {
					type : new GraphQL.GraphQLNonNull(GraphQLString)
				},
				name : {
					type : new GraphQL.GraphQLNonNull(GraphQLString)
				}
			},
			resolve : (rootValue, { tip_id }, { printer_id }, { name }) => {
				return new Promise ((resolve,reject) => {
					PrinterService.notifyTip(printer_id,name,tip_id, function(data){
						if(JSON.parse(data).codeReturn == "1"){
							resolve("success")
						}else{
							resolve("failure")
						}
					})
				})
			}
		},
		updateReservedFile : {
			type : GraphQLString,
			args : {
				avail_id : {
					type : new GraphQL.GraphQLNonNull(GraphQLString)
				},
				printer_id : {
					type : new GraphQL.GraphQLNonNull(GraphQLString)
				},
				new_name : {
					type : new GraphQL.GraphQLNonNull(GraphQLString)
				},
				new_validity : {
					type : new GraphQL.GraphQLNonNull(GraphQLString)
				}
			},
			resolve : (rootValue, { avail_id }, { printer_id }, { new_name }, { new_validity }) => {
				return new Promise ((resolve,reject) =>{
					var newReserved = {
						"name" : new_name,
						"newvalidity" : new_validity
					}
					PrinterService.updateReservedFile(printer_id,avail_id,newReserved,function(data){
						if(JSON.parse(data).codeReturn == "1"){
							resolve("success")
						}else{
							resolve("failure")
						}
					})
				})
			}
		}
  })
});

module.exports = PrinterMutation

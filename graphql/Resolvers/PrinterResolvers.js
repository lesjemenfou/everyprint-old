
const PrinterService = require('../dbservice/PrinterService')

module.exports = {
  /**
  ** Retrieves the printer's information from his id
  **/
  getById : (args,fn) => {
    PrinterService.getInfo(args.printer_id,function(StandartResult){
     // console.log('I am here')
     // console.log(JSON.parse(StandartResult).name)
      StandartResult = JSON.parse(StandartResult)
      // console.log(StandartResult)
      if(StandartResult.codeReturn == "1"){
        console.log("\nStandart :"+StandartResult)
        fn(StandartResult)

      }else{
        fn(null)
      }
    })

  },

  /**
  ** Retrive the printer's prices
  **/

  getPrice : (args, fn) => {
    PrinterService.getPrices(args.printer_id, function(Prices){
      Prices = JSON.parse(Prices)
      if(Prices.codeReturn == "1"){
        console.log("\nPrices : "+Prices)
      fn(Prices)
      }
    })
  },

  /**
  ** Retrives the tips of a printer
  **/

  getTips : (args,number,id,fn) => {
    if(number == null && id == null ){
      PrinterService.getTips(args.printer_id,function(result){
        result = JSON.parse(result)
        if(result.codeReturn == "1"){
          console.log("\nTips loading .....\n"+result.list)
          fn(result.tips)
        }
      });

    }else{

      if(number !=null && id == null){


       PrinterService.getTipsNumber(args.printer_id, number,function(result){
         result = JSON.parse(result.list)
         if(result.codeReturn == "1"){
           fn(result.tips)
         }
      });
 
      }else{
        PrinterService.getTipsById(id,function(result){
          result = JSON.parse(result)
          if(result.codeReturn == "1"){
            fn([result])
          }
        })
      }
    }
  },

  /**
  ** Get Balance history
  **/

  getBalanceHistory : (args,days,fn) => {
    if(days == null){
      PrinterService.getBalanceHistory(args.printer_id,function(result){
        result = JSON.parse(result)
        if(result.codeReturn == "1"){
          console.log(result)
          var history = []
          var historyLen = result.history.length
          for (i = 0; i<historyLen; i++){
            history.push({
              "date_time" : result.history[i].M.date_time.S,
              "retrieved" : result.history[i].M.retrieved.S
            })
          }
          fn(history)
          
        }
      });
    }else{
      PrinterService.getBalanceHistoryByNumber(args.printer_id,days,function(result){
        result = JSON.parse(result)
        if(result.codeReturn == "1"){
          fn(result.history)
        }
      });

    }
  },

  /**
  ** GetTipsStats by his id
  **/

  getTipsStats : (args,fn) => {
    console.log("Fetching "+args.tipid)

    PrinterService.getTipStats(args.tipid,function(result){
      result = JSON.parse(result)
      if(result.codeReturn == "1"){
        console.log("Tips Stats ..... \n"+result.reviews)
        fn(result.reviews)
      }
    });

  },

  /**
  ** Get PerdayStat
  **/

  getPerDayStat : (args,number,fn) => {
    if (number == null){
      PrinterService.getPerDayStat(args.printer_id, function(result){
        result = JSON.parse(result)
        if(result.codeReturn == "1"){
          fn(result.stats)
        }
      });
    }else{
      PrinterService.getPerDayStatNumber(args.printer_id,number,function(result){
        result = JSON.parse(result)
        if(result.codeReturn == "1"){
        fn(result.stats)
        }
      })
    }
  },

  /**
  ** Get Perdaystat for a given date
  **/

  getPerDayStatDay : (args,date,fn) => {

    PrinterService.getPerDayStatDate(args.printer_id,date,function(result){
          result = JSON.parse(result)
          if(result.codeReturn == "1"){
            fn(result.stats)
          }
    })

  },

  /**
  ** Get All available files
  **/

  getAllAvailFiles : (args,fn) => {
    PrinterService.getAvailsAllById(args.printer_id,function(result){
      result = JSON.parse(result)
      if(result.codeReturn == "1"){
      fn(result.files)
      }
    });
  },

  /**
  ** Get All available files
  **/

  getNumberAvailFiles : (args,number,fn) => {
    PrinterService.getAvailsNumberById(args.printer_id,number,function(result){
      result = JSON.parse(result)
      if(result.codeReturn == "1"){
        fn(result.files)
      }
    });
  }



}

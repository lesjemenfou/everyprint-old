
const CustomerService = require('../dbservice/CustomerService')

module.exports = {
  /**
  ** Get info about a customer
  **/

  getCustomerInfo : (args,fn) => {
      CustomerService.getInfo(args.customer_id,function(result){
        result = JSON.parse(result)
      if(result.codeReturn == "1"){
        fn(result)
      }else{
        fn(null)
      }
    })
  },

  /**
  **Get feedbacks
  **/

  getFeedBacks : (args, fn) => {
    CustomerService.getFeedback(args.customer_id, function(result){

      if(result.codeReturn = "1"){
        fn(result.feedbacks)
      }
    })
  },

  addTipFeedback : (customerId, tip_id, feedback,fn) => {
    CustomerService.addTipFeedback(customerId, tip_id, feedback,function(result){
        result = JSON.parse(result)
        if(result.codeReturn == "1"){
          fn(result)
        }else{
          fn(null)
        }
    })
  },
  saveCustomer : (customerId,last_name,gender,fn) => {
    var customer = {
      "messenger_id" : customerId,
      "last_name" : last_name,
      "gender" : gender
    }

    CustomerService.save(customer,function(result){
      result = JSON.parse(result)
      if(result.codeReturn == "1"){
        fn(result)
      }else{
        fn(null)
      }
    })
  }


}


const CommandService = require('../dbservice/CommandService')
const request = require('request')
const config = require('../../config.json')



module.exports = {
  /**
  ** Get commands by printer id
  **/

  getCommandsById : (args,fn) => {
    console.log("command for :"+args.printer_id)
    CommandService.getCommandsForPrinterAll(args.printer_id, function(result){
          result = JSON.parse(result)
          if(result.codeReturn == "1"){
            
            fn(result.commands)
          }
    })


  },

  getCommandsByState : (args,state, fn) => {
    CommandService.getCommandsForPrinterState(args.printer_id,state, function(result){
          result = JSON.parse(result)
          if(result.codeReturn == "1"){
            fn(result.commands)
          }else{
            fn(null)
          }
    })

  },

  computeCommand : (commandOptions, location, fn) => {
    console.log("saving command .....")

    CommandService.save(commandOptions.color, commandOptions.copies, commandOptions.messengerid,commandOptions.url,commandOptions.pages_to_print,commandOptions.paper_format,commandOptions.grouping,commandOptions.rectoverso,commandOptions.name,function(data){
      var result = JSON.parse(data)
      if(result.codeReturn == "1"){
        var command_id = result.command_id
        console.log("compute command "+command_id)
        CommandService.computeCommand(command_id,location,function(compute){
          compute = JSON.parse(compute)
          if(compute.codeReturn == "1"){
            fn(JSON.stringify({
              "messages" : compute.messages
            }))
          }
        })

      }
    })
  },

  retrieveCommand : (command_id, fn) => {
    CommandService.updateRetrievedTime(command_id,new Date().now(),function(res){
      fn(JSON.stringify({
        "codeReturn" : "1"
      }))
    })
  },

  updateState : (command_id, state,fn) => {
    CommandService.changeState(command_id, state, function(result){
      if(JSON.parse(result).codeReturn == "1"){
          fn(JSON.stringify({
            "codeReturn" : "1"
          }))
      }else{
        fn(JSON.stringify({
          "codeReturn" : "-1",
          "error" : "Couldn't achieve update"
        }))
      }
    })
  },
 
  achieveCommand : (command_id,customer_id,printer_name,fn) => {
    CommandService.changeState(command_id, "2",function(resState){
      CommandService.updateAchievedTime(command_id,new Date().now(),function(resAch){
        //Send the notification to the user 
        var params = {
          url : "https://api.chatfuel.com/bots/"
                + config.EVEYPRINT_BOT_ID +"/users/"
                + customer_id + "/send",
          method : 'POST',
          qs : {
            "chatfuel_token" : config.CHATFUEL_API_KEY,
            "chatfuel_block_name" : config.EVERYPRINT_COMMAND_OK,
            "printer_name" : printer_name,
            "command_id" : command_id
          }
        }


        request(params, function(error, response, body){
          if(error){
            fn(JSON.stringify({
              "codeReturn" : "0",
              "message" : "Error during post",
              "body" : body
            }));
        }
      })

      })
    })
  },

  sendPrice : (color,copies,customer_id,file_url,pages_to_print,paper_format,grouping,name,rectoverso,fn) => {
    console.log("Sending price ... ")
    CommandService.save(color,copies,customer_id,file_url,pages_to_print,paper_format,grouping,rectoverso,name,function(data){
      
      var result = JSON.parse(data)
      if(result.codeReturn == "1"){
        console.log("command saved correctly command id "+result.command_id)
        /**
         * Compute Price and send to the customer 
         */
        /**
         * For launch purpose, the prices is defined here as a constant
         * As for the printer's location 
         */
        const printer_email = "djob.mvondo@3islabs.com"
        const printer_name = "LE PARRAIN"
        const printer_number = "677378688"
        const prices = {
          "color_A3": 100,
          "color_A4": 15,
          "color_A5": 75,
          "color_rectoverso": 10,
          "gray_A3": 75,
          "gray_A4": 10,
          "gray_A5": 50,
          "gray_rectoverso": 20,
          "rayure": 250,
          "serseau": 200
        }
         const fee = "5"
         const sentTime = new Date(Date.now()).toLocaleString()
         var command = {
           "command_id" : result.command_id, 
           "url" : file_url,
           "color" : color,
           "copies" : copies,
           "customer_id" : customer_id,
           "pages_to_print" : pages_to_print,
           "paper_format" : paper_format,
           "grouping" : grouping,
           "paid" : "No",
           "sent" : sentTime,
           "name" : name,
           "rectoverso" : rectoverso,
           "balance" : ""
         }

         CommandService.computePriceDemo(prices,command,function(priceResult){
           //Update the price of the command 
           var price = JSON.parse(priceResult)
           console.log("\nPrice Computed "+price+" , price-- : "+price.price)
           CommandService.changeIncome(result.command_id,price.price,function(res){
            CommandService.changeSentTime(result.command_id,sentTime,function(resTime){
              CommandService.changeFee(result.command_id,fee,function(resFee){
                CommandService.changeState(result.command_id, "0",function(resState){

                  command.balance = price.price 
                  
                  //Send the email to the printer 
                  
                  console.log("Email senttt")

                  var set_attributes = {
                    "printer_name" : printer_name,
                    "command_id" : result.command_id,
                    "price" : price.price,
                    "priceExplain" : price.priceExplain,
                    "printer_number" : printer_number
                  }
                  
                  var messages = [
                    {
                      "text" : "Votre commande a bien été receptionné par le *"+printer_name+"*"+
                               "\nCela vous coutera *"+price.price+"*FCFA\n (*Details*: "+price.priceExplain+")"
                    },
                    {
                      "text" : "Pour retirer votre commande, veuillez présentez ce numéro de commande : "+result.command_id 
                    },
                    {
                      "text" : "Voulez vous reglez la commande maintenant ? (Si non, vous paierez au retrait)\n"+
                                "NB: Si non, l'impression est effectué après que le payement soit éffectué sur place",
                      "quick_replies" : [
                        {
                          "title" : "Oui",
                          "block_names" : ["AskPayMethod-block"]
                        },
                        {
                          "title" : "Non",
                          "block_names" : ["PayCash"]
                        },
                        {
                          "title" : "Annuler la commande",
                          "block_names" : ["Cancel-pay"]
                        }
                      ]
                    }
                  ]
                  console.log("Messages : "+messages+" \n Set_attributes "+set_attributes)
                

              CommandService.sendEmail(result.command_id,printer_email,command, function(emailSent,valReturn){
                  //Send the price to the customer 
                  var params = {
                    url : "https://api.chatfuel.com/bots/"
                          + config.EVEYPRINT_BOT_ID +"/users/"
                          + customer_id + "/send",
                    method : 'POST',
                    qs : {
                      "chatfuel_token" : config.CHATFUEL_API_KEY,
                      "chatfuel_block_name" : config.EVERYPRINT_PAYDEMO_BLOCK,
                      "printer_name" : printer_name,
                      "price" : price.price,
                      "printer_number" : printer_number,
                      "command_id" : result.command_id 
                    }
                  }

                  request(params, function(error, response, body){
                    if(error){
                      fn(JSON.stringify({
                        "codeReturn" : "0",
                        "message" : "Error during post",
                        "body" : body
                      }));
                  }else{
                    /*
                    * For test purposes, set printerIds to xxxxxxx
                    */
                    CommandService.changePrinterId(result.command_id,"xxxxxxx",function(changedPrinter){
                      changedPrinter = JSON.parse(changePrinterId)
                      if(changedPrinter.codeReturn == "1"){
                        console.log("Could successfully update the printer's id")
                      }else{
                        console.log("Couldn't update the printer's id for the command")
                      }
                      fn(JSON.stringify({
                        "codeReturn" : "1",
                        "set_attributes" : set_attributes, 
                        "messages" : messages
                      }))
                    })
                    //Send Email to printer 
                   
                    
                  }
                  })
                  
                  
              })
                
          
                  
                
  
                })
              })

            })
           })
           
           
           
         })
      }else{
        console.log("Couldn't send price")
      }
    })
  }
}

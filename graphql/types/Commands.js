const GraphQL = require('graphql');
const {
	GraphQLObjectType,
	GraphQLString,
	GraphQLID,
	GraphQLInt,
  GraphQLFloat,
  GraphQLNonNull
} = GraphQL;

const PrinterType = require('./Printer')
const CustomerType = require('./Customer')

const CustomerResolver = require('../Resolvers/CustomerResolver')


const CommandType = new GraphQL.GraphQLObjectType({
    name : "Command",
    description : "The entity specifying a command",
    fields : () => ({
      command_id : {
        type : new GraphQL.GraphQLNonNull(GraphQLID),
        description : "The id of command"
      },
      customer : {
        type : CustomerType,
        description : "The customer who issued the command",
				resolve : (command) => {
					return new Promise((resolve,reject) => {
						CustomerResolver.getCustomerInfo(command,function(result){
							resolve(result)
						});

					})
				}
      },
      printer_id : {
        type : GraphQLString,
        description : "The printer who issued the command"
      },
      income : {
        type : GraphQLFloat,
        description : "The price of the command"
      },
      fee : {
        type : GraphQLString,
        description : "The fee charged to the command"
      },
      copies : {
        type : GraphQLInt,
        description : "The number of copies for the command"
      },
      file_url : {
        type : GraphQLString,
        description : "The url of the file to print"
      },
      pages_to_print : {
        type : GraphQLString,
        description : "The pages to print"
      },
      grouping : {
        type : GraphQLString,
        description : "The type of grouping the customer wishes"
      },
      paper_format : {
        type : GraphQLString,
        description : "The type of format on which the print should be done"
      },
      sent_time : {
        type : GraphQLString,
        description : "The time at which the command was sent"
      },
      color : {
        type : GraphQLString,
        description : "Yes/No should we print in color"
      },
      state : {
        type : GraphQLString,
        description : "The state of the command"
      },
      achieved_time : {
        type : GraphQLString,
        description : "The time at which the command was done"
      },
      retrieved_time : {
        type : GraphQLString,
        description : "The time at which the customer retrieved the command"
      },
			rectoverso : {
				type : GraphQLString,
				description : "Yes/no do we wish rectoverso printing"
			},
      name : {
        type : GraphQLString,
        description : "The name of the person who will retrieve"
      }
    })
});
module.exports = CommandType

const GraphQL = require('graphql');
const {
	GraphQLObjectType,
	GraphQLString,
	GraphQLID,
	GraphQLInt,
  GraphQLFloat,
  GraphQLNonNull
} = GraphQL;

const CustomerResolver = require('../Resolvers/CustomerResolver')

const BotFeedbacks = new GraphQL.GraphQLObjectType({
    name : "Feedbacks",
    description : "The feedbacks given by this customer",
    fields : () => ({
        customer : {
          type : new GraphQL.GraphQLNonNull(CustomerType),
          description : "The messenger id of the customer of these feedbacks"
        },
        rating : {
          type : GraphQLInt,
          description : "The rating given by this customer"
        },
        comment : {
          type : GraphQLString,
          description : "The comment given by the customer to the App"
        },
        date : {
          type : GraphQLString,
          description : "The date of the given rating"
        }
    })
});



const CustomerType = new GraphQL.GraphQLObjectType({
    name : "Customer",
    description : "A customer willing to order a command",
    fields : () => ({
         customer_id : {
          type : new GraphQL.GraphQLNonNull(GraphQLID),
          description : "The messenger id of the customer "
        },
        last_name : {
          type : GraphQLString,
          description : "The last name of the customer"
        },
        gender : {
          type : GraphQLString,
          description : "The gender of the customer"
        },
        telephone : {
          type : GraphQLString,
          description : "The telephone number of the customer"
        },
        balance : {
          type : GraphQLString,
          description : "The balance of the customer"
        },
        feedbacks : {
          type : new GraphQL.GraphQLList(BotFeedbacks),
          description : "The bot feedbacks given by the customer",
					resolve : (customer) => {
						return new Promise ((resolve,reject) => {
							CustomerResolver.getFeedbacks(customer,function(result){
								resolve(result)
							});
						})

					}
        }
    })
});

module.exports = CustomerType

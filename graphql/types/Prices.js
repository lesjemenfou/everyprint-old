
const GraphQL = require('graphql');
const {
	GraphQLObjectType,
	GraphQLString,
	GraphQLID,
	GraphQLInt,
  GraphQLFloat
} = GraphQL;

const Prices = new GraphQL.GraphQLObjectType({
    name : "Prices",
    description : "This represents the differents prices offered by this printer",
    fields : () => ({
        colorA4 : {
            type : GraphQLFloat,
            description : "The price for A4 color / page"
        },
        colorA3 : {
            type : GraphQLFloat,
            description : "The price for A3 color / page"
        },
        colorA5 : {
             type : GraphQLFloat,
             description : "The price for A5 color /page"
        },
        colorRecto : {
              type : GraphQLFloat,
              description : "The price to add for rectoverso in color mode"
        },
        grayA4 : {
            type : GraphQLFloat,
            description : "The price for A4 gray / page"
        },
        grayA3 : {
            type : GraphQLFloat,
            description : "The price for A3 gray / page"
        },
        grayA5 : {
             type : GraphQLFloat,
             description : "The price for A5 gray /page"
        },
        grayRecto : {
              type : GraphQLFloat,
              description : "The price to add for rectoverso in gray mode"
        },
        rayure : {
              type : GraphQLFloat,
              description : "The price for a rayure"
        },
        serseau : {
              type : GraphQLFloat,
              description : "The price for a serseau"
        }

    })
});

module.exports = Prices;

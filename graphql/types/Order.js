const GraphQL = require('graphql');
const async = require('async')
const {
	GraphQLObjectType,
	GraphQLString,
	GraphQLID,
	GraphQLInt,
  GraphQLFloat,
  GraphQLNonNull,
  GraphQLList
} = GraphQL;

const PrinterType = require('./Printer')
const CustomerType = require('./Customer')
const CommandType = require('./Commands')

const CommandService = require('../dbservice/CommandService')
const CustomerResolver = require('../Resolvers/CustomerResolver')

const orderType = new GraphQL.GraphQLObjectType({
    name : "Order",
    description : "An order constitue of a list of commands",
    fields : () => ({
        id_order : {
          type : new GraphQL.GraphQLNonNull(GraphQLString),
          description : "The id of the order"
        },
        income : {
          type : GraphQLFloat,
          description : "The income of an order"
        },
        customer : {
           type : CustomerType,
       	   description : "The customer who issued the command",
				resolve : (order) => {
					return new Promise((resolve,reject) => {
						CustomerResolver.getCustomerInfo(order,function(result){
							resolve(result)
						});

					})
				}
        }, 
        state : {
      	  type : GraphQLInt,
        	description : "The state of the order"
	    },
	    achieved_time : {
	      	type : GraphQLString,
	        description : "The time at which the order was done"
	    },
	    retrieved_time : {
	        type : GraphQLString,
	        description : "The time at which the customer retrieved the order"
	    },
	    sent_time : {
	    	type : GraphQLString,
	    	description : "The sent time of the order"
	    },
	    commands : {
	    	type : GraphQLList(CommandType),
	    	description : "The list of commands contained in the order",
	    	resolve : (order) => {
	    		return new Promise ((resolve,reject) => {
	    			if(order == null || order.commands == undefined){
	    				resolve([null])
	    			}else{
	    				async.map(order.commands,function(command,callback){
	    				CommandService.getCommandById(command,function(result){
	    					result = JSON.parse(result)
	    					if(result.codeReturn == "1"){
	    						callback(null,result)
	    					}else{
	    						callback(null,null)
	    					}
	    				})

	    			},function(err,result){
	    				resolve(result)
	    			})	
	    			}
	    			
	    		})
	    	}
	    }
    })
});

module.exports = orderType
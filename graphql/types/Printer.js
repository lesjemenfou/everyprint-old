const GraphQL = require('graphql');
const {
	GraphQLObjectType,
	GraphQLString,
	GraphQLID,
	GraphQLInt,
  GraphQLFloat,
	GrapqhQLNonNull,
	GraphQLList
} = GraphQL;

const PrinterResolver = require('../Resolvers/PrinterResolvers')
const CommandResolver = require('../Resolvers/CommandResolver')
const CommandService = require('../dbservice/CommandService')
const Prices = require('./Prices')
const Commands = require('./Commands')
const Orders = require('./Order')

const BalanceHistory = new GraphQL.GraphQLObjectType({
    name : "BalanceHistory",
    description : "The retrieval operations made by a printer",
    fields : () => ({
        date_time : {
          type : GraphQLString,
          description : "The date of the retrieval done"
        },
        retrieved : {
          type : GraphQLInt,
          description : "The amount retrieved"
        }
    })
});

const AvailableFiles = new GraphQL.GraphQLObjectType({
    name : "AvailableFiles",
    description : "The files available from a printer",
    fields : () => ({
				avail_id : {
				type : GraphQLString,
				description : "The id of the available file"
				},
        printer_id : {
          type : new GraphQL.GraphQLNonNull(GraphQLInt),
          description : "The printer's id for which we wish the available files"
        },
        file_url : {
          type : GraphQLString,
          description : "The AWS S3 url of the file"
        },
        reserve : {
          type : GraphQLInt,
          description : "The number of reservation of the file done"
        },
        name : {
          type : GraphQLString,
          description : "The name of the reserved files"
        },
				validity : {
					type : GraphQLString,
					description : "The validity end of the file"
				}
    })
});

const PerDayStat = new GraphQL.GraphQLObjectType({
    name : "PerDayStat",
    description : "The per day statistics of the commands",
    fields : () => ({
        day : {
          type : GraphQLString,
          description : "The statistics for the day"
        },
        commands_done :  {
          type : GraphQLInt,
          description : "The number of commands of achieved this day"
        },
        balance : {
          type : GraphQLFloat,
          description : "The balance achived this day"
        }
    })
});

const TipsStats = new GraphQL.GraphQLObjectType({
	name : "TipsStats",
	description : "The statistics for a given tip",
	fields : () => ({
		goodReviews : {
			type : GraphQLString,
			description : "The number of good reviews given by customers to the tip"
			},
		neutralReviews : {
			type : GraphQLString,
			description : "The number of neutral reviews given by customers to the tip"
		},
		badReviews : {
			type : GraphQLString,
			description : "The number of bad reviews given by customers to the tip"
		}
	})
});

const Tips = new GraphQL.GraphQLObjectType({
	name : "Tips",
	description : "This represent the tips issued to customers by printers",
	fields : () => ({
			tipid : {
				type : GraphQLString,
				description : "The id of the tip"
			},
			description : {
				type : GraphQLString,
				description : "The description of the tip"
			},
			tip_url : {
				type : GraphQLString,
				description : "The url of the tip (video, pdf, etc ....)"
			},
			name : {
				type : GraphQLString,
				description : "The name of the tip"
			},
			stats : {
				type : TipsStats,
				description : "The stats about this tip",
				resolve : (tips) => {
					return new Promise((resolve,reject)=>{
						PrinterResolver.getTipsStats(tips,function(result){
							resolve(result)
						});
					})

				}
			}
	})
});

const PrinterType = new GraphQL.GraphQLObjectType({
    name : "Printer",
    description : "This represents an available printer",
    fields : () => ({
        printer_id : {
          type: GraphQLString,
          description : "The id of the printer in the dynamo Table"
        },
        name : {
          type : GraphQLString,
          description : "The name of the printer registered"
        },
        telephone : {
          type : GraphQLString,
          description : "The telephone number of the printer"
        },
        mail : {
          type : GraphQLString,
          description : "The mail of the printer"
        },
        balance : {
          type : GraphQLFloat,
          description : "The actual balance of the printer"
        },
        logo_url : {
          type : GraphQLString,
          description : "The actual logo of the printer"
        },
        available : {
          type : GraphQLInt,
          description : "The availability of the printer"
        },
        location : {
          type : GraphQLString,
          description : "The location of the of the printer"
        },
        balance_history : {
          type : new GraphQLList(BalanceHistory),
          description : "The balance history of the printer",
					args : {
						days : {
							name : "days",
							description : "The number of retrieval to give backwards",
							type : GraphQLInt
						}
					},
					resolve : (printer,{ days }) => {
						return new Promise((resolve,reject)=>{
							PrinterResolver.getBalanceHistory(printer,days,function(result){
								console.log(result)
								resolve(result)
							});
						})

					}
        },
        available_files : {
          type : new GraphQL.GraphQLList(AvailableFiles),
          description : "The available files for this printer",
					args : {
						number : {
							name : "The number of available files",
							type : GraphQLInt
						}
					},
					resolve : (printer, { number }) => {
						return new Promise((resolve,reject) => {
							if(number != null) PrinterResolver.getNumberAvailFiles(printer,number,function(result){
								resolve(result)
							});
							PrinterResolver.getAllAvailFiles(printer,function(result){
								resolve(result)
							});
						})

					}
        },
        per_day_stat : {
          type : new GraphQL.GraphQLList(PerDayStat),
          description : "The statistics for previous days",
					args : {
						number : {
							name : "number",
							description : "The number of days backwards",
							type : GraphQLInt
						},
						date : {
							name : "date",
							description : "The date of the stat",
							type : GraphQLString
						}
					},
					resolve : (printer, { number }, { date }) => {
						return new Promise((resolve,reject)=>{
							if(number == null && date != null){
								PrinterResolver.getPerDayStatDay(printer,date,function(data){
									resolve(data)
								})
							}else if (number != null && date == null){
								PrinterResolver.getPerDayStat(printer,number, function(data){
									resolve(data)
								})
							}else PrinterResolver.getPerDayStat(printer,null,function(data){
									resolve(data)
							})
						})

					}
        },
        prices : {
          type : Prices,
          description : "The prices for this printer",
					resolve : (printer) => {
						return new Promise((resolve,reject) => {
							PrinterResolver.getPrice(printer,function(data){
								resolve(data)
							})
						})

					}
        },
				tips : {
					type : new GraphQL.GraphQLList(Tips),
					description : "The tips for this printer",
					args : {
						number : {
							name : "number",
							description : "the number of tips to give",
							type : GraphQLInt
						},
						id : {
							name : "tip_id",
							description : "The id of the tip",
							type : GraphQLString 
						}
					},
					resolve : (printer,args) => {
						return new Promise((resolve,reject) => {
							PrinterResolver.getTips(printer,args.number,args.id,function(data){
								resolve(data)
							});
						})

					}
				},
				commands : {
					type : new GraphQLList(Commands),
					description : "The list of the commands",
					args : {
						state : {
							name : "state",
							description : "the state of the commands",
							type : GraphQLString
						}
					},
					resolve : (printer, { state }) => {
						return new Promise((resolve,reject)=>{
							if (state != null) {
								CommandResolver.getCommandsByState(printer,state,function(data){
									console.log("commands\n"+data)
									resolve(data)
								})
							}else{
								CommandResolver.getCommandsById(printer,function(data){
									console.log("commands\n"+data)
									resolve(data)
								})
							}
						})

					}
				},

				orders : {
					type : new GraphQLList(Orders),
					description : "The list of orders for the printer",
					args : {
						state : {
							name : "state",
							description : "the state of the orders",
							type : GraphQLString
						}
					},
					resolve : (printer, { state }) => {
						return new Promise((resolve,reject) => {
							if(state != null){
								CommandService.getOrdersForPrinterState(printer.printer_id, state, function(data){
									resolve(JSON.parse(data).orders)
								})
							}else{
								CommandService.getOrdersForPrinterAll(printer.printer_id, function(data){
									resolve(JSON.parse(data).orders)
								})
							}
						})
					}
				}
    })
});

module.exports = PrinterType
